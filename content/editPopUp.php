<?php require_once('session.php'); ?>
<body onload='javascriptPages.popUp();'>    
    <div class='mainCont'>
    <div class="closeMark"></div>
    <?php
        $input = new inputBox;
        $input->setId('openAllFields');
        $input->setValue($language->general->openAll);
        $input->setType('button');
        echo $input->createInput();
        
        if($_SESSION['userInfo']['superUser'] == 'yes'){ 
            $input = new inputBox;
            $input->setId('forceFileOpen');
            $input->setValue($language->general->forceFileOpen);
            $input->setType('button');
            //if ($row['open']==0)
                //$input->setDisabled('disabled');
            echo $input->createInput();
        }
        
        $form = new form;
        $form->setForm($formArray->getMainForm());
        $form->setRowNum(6);
        $form->setFormClass('editDataForm');
        $form->setTableClass('editDataForm');
        $form->setTitle($language->edit->editForm);
        echo $form->createForm();
    ?>
    </div>
</body>
</html>