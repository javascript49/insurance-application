<?php require_once('header.php'); ?>
<body onload='javascriptPages.login();'>
    <div id="loginHeaderLogo">
        <div id="Logos">
            <?php require('./footer.php') ?>
        </div>
    </div>
    <div id="loginCont">
        <div class='loginBox'>
            <?php
            
            $form = new form;
            $form->setForm($formArray->getLoginForm());
            $form->setName('Login');
            $form->setMethod('POST');
            $form->setTitle($language->login->login);
            echo $form->createForm();
            ?>
            <div class="serverResponse" id="loginResponse">
            <?php if (isset($requestHandler->dataResponse))
                    echo $requestHandler->dataResponse; ?>
            </div>
        </div>
    </div>
 
</body>
</html>