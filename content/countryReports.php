<?php require_once('session.php'); ?>
<body onload='javascriptPages.mainReportsCo();'>
    <?php require_once('menu.php'); ?>
    <div class='mainCont'>
        <?php
        
        $reportForm = new form;
        $reportForm->setForm($formArray->getCountryReportsForm());
        $reportForm->setName('countryReport');
        $reportForm->setMethod('POST');
        $reportForm->setTitle($language->reports->countryReport);
        echo $reportForm->createForm();
        
        ?>
    </div>
</body>
</html>