<?php
ob_start();
header('Content-Type: text/html; charset=windows-1255'); 
require_once("../class/multiLang/lang.php");
$lang = new lang;
$lang->setLang( isset($_SESSION['userInfo']['lang'])?$_SESSION['userInfo']['lang']:'eng');
$language = $lang->createMultiLang();

require_once("../class/handlers/requestHandler.php");
require("../class/dataBase/dataBaseHandle.php");
require("../class/form/uploadFile.php");
require_once '../class/excel/PHPExcel/IOFactory.php';
require_once("../class/form/inputBox.php");
require_once("../class/form/selectBox.php");
require_once("../class/form/textArea.php");
require_once("../class/form/form.php");
require_once("../class/handlers/setForms.php");
$requestHandler = new requestHandler;
$requestHandler->language = $language;
$formArray = new setForms();
$formArray->setLanguage($language);
$formArray->setRequestHandler($requestHandler);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html dir="<?php echo $language->general->direction ?>" style="float:<?php echo $language->general->floatDir ?>;">
<head>
    <title></title>
    
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=HEBREW">
    <meta HTTP-EQUIV="content-type" CONTENT="text/html; charset=windows-1255" />
    <META HTTP-EQUIV="Content-language" CONTENT="he">
    <link rel="stylesheet" href="../style/style.css">
    <script>
        var message = {
            areYouSure : "<?php echo $language->general->areYouSure ?>",
            closeEdit  : "<?php echo $language->general->closeEdit  ?>",
            areYouSureOpen : "<?php echo $language->general->openRedit  ?>",
            dateAlert   :   "<?php echo $language->general->dateAlert  ?>",
            buttonOne   :   "<?php echo $language->general->buttonOne  ?>",
            buttonSec   :   "<?php echo $language->general->buttonSec  ?>",
            submitExport   :   "<?php echo $language->general->submitExport  ?>",
            noData   :   "<?php echo $language->general->noData  ?>"
        }
    </script>
    <script src="../js/jquery1.9.js"></script>
    <script src="../js/main.js"></script>
</head>
