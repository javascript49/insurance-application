<?php
session_start();
session_unset($_SESSION['userInfo']);
session_destroy();
header('Location: index.php');
?>