<?php require_once('session.php'); ?>
<?php if($_SESSION['userInfo']['superUser'] == 'yes'){ ?>
    <body onload='javascriptPages.admin();'>
        <?php require_once('menu.php'); ?>
    
        <div class='mainCont'>
            <?php
            
            $menu = new form;
            $menu->setForm($formArray->getMainOpMenuForm());
            $menu->setName('mainOpForm');
            $menu->setMethod('POST');
            $menu->setTitle($language->menu->operation);
            echo $menu->createForm();
            
            $form0 = new form;
            $form0->setForm($formArray->getChooseToEditForm());
            $form0->setName('ChooseToEdit');
            $form0->setMethod('POST');
            echo $form0->createForm();
            
            $form1 = new form;
            $form1->setForm($formArray->getAddAgentForm());
            $form1->setName('addUser');
            $form1->setMethod('POST');
            $form1->setFormClass('adminForms');
            $form1->setTitle($language->dealerOperation->menuAdd);
            echo $form1->createForm();
            
            $form2 = new form;
            $form2->setForm($formArray->getblockMenuForm());
            $form2->setName('blockUser');
            $form2->setMethod('POST');
            $form2->setFormClass('adminForms');
            $form2->setTitle($language->dealerOperation->menuBlock);
            echo $form2->createForm();
            
            $form3 = new form;
            $form3->setForm($formArray->getopenMenuForm());
            $form3->setName('openUser');
            $form3->setMethod('POST');
            $form3->setFormClass('adminForms');
            $form3->setTitle($language->dealerOperation->menuOpen);
            echo $form3->createForm();
            
            $form4 = new form;
            $form4->setForm($formArray->getEditAgentForm());
            $form4->setName('editUser');
            $form4->setMethod('POST');
            $form4->setFormClass('adminForms');
            $form4->setTitle($language->dealerOperation->menuEdit);
            echo $form4->createForm();
            
            $form5 = new form;
            $form5->setForm($formArray->getReportsForm());
            $form5->setName('reports');
            $form5->setMethod('POST');
            $form5->setFormClass('adminForms');
            $form5->setTitle($language->dealerOperation->reports);
            echo $form5->createForm();
            
            $form6 = new form;
            $form6->setForm($formArray->getExportForm());
            $form6->setName('export');
            $form6->setMethod('POST');
            $form6->setFormClass('adminForms');
            $form6->setTitle($language->dealerOperation->export);
            echo $form6->createForm();
            ?>
            <form method="POST" class="adminForms" name="uploadFile" enctype="multipart/form-data" >
                <?php
                
                $form7 = new form;
                $form7->setForm($formArray->getUploadForm());
                $form7->setHideFormTag(true);
                $form7->setName('uploadFile');
                $form7->setMethod('POST');
                $form7->setTitle($language->general->chooseFile);
                echo $form7->createForm();
                ?>
            </form>
        </div>
        <div class='loggerRes'>
            <?php if (isset($requestHandler->logger)){?>
                <a id="loggerOpen" loggerRes="<?php echo $requestHandler->logger ?>">Click Here To View The Logger And find if everything went well </a>
            <?php }?>
        </div>
        <?php require('./footer.php') ?>
    </body>
    </html>
<?php } ?>