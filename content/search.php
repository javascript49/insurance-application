<?php require_once('session.php'); ?>
<body onload='javascriptPages.mainSearch();'>
    <?php require_once('menu.php'); ?>
    <div id="loadOverLay"><div class="loadSymbol"></div></div>
    <div class='ajaxCont'></div>
    <div class='mainCont'>
        <?php
        
        $form2 = new form;
        $form2->setMethod('POST');
        $form2->setAction('search.php');
        $form2->setForm($formArray->getMainSearchForm());
        $form2->setTableClass('mainSearchForm');
        $form2->setFormId('mainSearchData');
        $form2->setTitle($language->mainForm->mainForm);
        echo $form2->createForm();
                    
        echo "<table class='searchResultTab'>";
            echo "<thead>";
                echo "<tr>";
                echo "<td>".$language->mainForm->id."</td>";
                echo "<td>".$language->mainForm->agentId."</td>";
                echo "<td>".$language->mainForm->insNum."</td>";
                echo "<td>".$language->mainForm->indicator."</td>";
                echo "<td>".$language->mainForm->insName."</td>";
                echo "<td>".$language->mainForm->insId."</td>";
                echo "<td>".$language->mainForm->sDate."</td>";
                echo "<td>".$language->mainForm->eDate."</td>";
                echo "<td>".$language->mainForm->relate."</td>";
                echo "<td>".$language->mainForm->licenseNum."</td>";
                echo "<td>".$language->mainForm->insComp."</td>";
                echo "<td>".$language->mainForm->status."</td>";
                echo "<td>".$language->mainForm->comments."</td>";
                echo "<td>".$language->mainForm->handlerName."</td>";
                echo "<td>".$language->mainForm->open."</td>";
                echo "<td>".$language->mainForm->openName."</td>";
                echo "<td>".$language->mainForm->date."</td>";
                echo "<td>".$language->mainForm->commentsFree."</td>";
                echo "</tr>";
                echo "</thead>";
                
                echo "<tbody><tr><td colspan='7'><div class='scrollerTable'><table style='width: 103%;overflow-x:hidden'>";
                if (!$requestHandler->dataResponse){
                    echo "<div id='noDataMessage'>".$language->general->noData."</div>";
                }
                if (isset($requestHandler->dataResponse)&&($requestHandler->dataResponse == 'success')){
                    $date = date("Y-m-d H:i:s",mktime(date("H"), date("i")-20, date("s") , date("m"),date("d") , date("Y")));
                    foreach($requestHandler->excelResponse as $row){
                        if (($row['open'] == 0)||(strtotime($row['date']) < strtotime($date))){
                            $row['open'] = 0;
                        }
                        
                        echo "<tr class='openForEdit' num='".$row['id']."'>";
                            echo "<td>".$row['id']."</td>";
                            echo "<td>".$row['agentId']."</td>";
                            echo "<td>".$row['insNum']."</td>";
                            echo "<td>".$row['indicator']."</td>";
                            echo "<td>".$row['insName']."</td>";
                            echo "<td>".$row['insId']."</td>";
                            echo "<td>".$row['sDate']."</td>";
                            echo "<td>".$row['eDate']."</td>";
                            echo "<td>".$row['relate']."</td>";
                            echo "<td>".$row['licenseNum']."</td>";
                            echo "<td>".$row['insComp']."</td>";
                            echo "<td>".$row['status']."</td>";
                            echo "<td>".$row['comments']."</td>";
                            echo "<td>".$row['handlerName']."</td>";
                            echo "<td>".(($row['open']==0)?$language->general->no:$language->general->yes)."</td>";
                            echo "<td>".$row['openName']."</td>";
                            echo "<td>".$row['date']."</td>";
                            echo "<td>".$row['commentsFree']."</td>";
                        echo "</tr>";
                       
                    }
                }
            echo "</table></div></td></tr></tbody>";
        echo "</table>";
            
        ?>
    </div>
    <?php require('./footer.php') ?>
</body>
</html>