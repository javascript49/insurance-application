<?php require_once('session.php'); ?>
<body onload='javascriptPages.reports();'>
    <?php
        echo '<h2>'.$language->reports->reportsHeader.' '.$_REQUEST['byMonth'].'</h2>';
        $input = new inputBox;
        $input->setId('printReport');
        $input->setValue($language->reports->printRep);
        $input->setType('button');
        echo $input->createInput();
        
        $countTotalRow = $countTotalRenew = $countTotalRenew = $countNone = $countCancel = $countMonthlyOther = 0;
        if (isset($requestHandler->dataResponse)&&($requestHandler->dataResponse == 'success')){
            foreach ($requestHandler->reports as $rowCount){
                $countTotalRow = $rowCount['totalRow'] + $countTotalRow;
                $countTotalRenew = $rowCount['totalRenew'] + $countTotalRenew;
                $countTotalUnRenew = $rowCount['totalUnRenew'] + $countTotalUnRenew;
                $countNone = $rowCount['none'] + $countNone;
                $countCancel = $rowCount['cancel'] + $countCancel;
                $countMonthlyOther = $rowCount['monthlyOther'] + $countMonthlyOther;
            }
        }
    ?>
    <div class='mainCont'>
        <?php
        if ($_REQUEST['reportType']=='report1'){
            echo "<table class='searchReportTab'>";
                echo "<thead>";
                    echo "<tr>";
                    echo "<td>".$language->reports->agentId."</td>";
                    echo "<td>".$language->reports->totalRow."</td>";
                    echo "<td>".$language->reports->totalRenew."</td>";
                    echo "<td>".$language->reports->difference."</td>";
                    echo "<td>".$language->reports->percent."</td>";
                    echo "</tr>";
                    echo "</thead>";
                    
                    echo "<tbody>";
                    if (!$requestHandler->reports){
                        echo "<tr ><td colspan='8'><div id='noDataMessage'>".$language->general->noData."</div><td></tr>";
                    }
                    if (isset($requestHandler->dataResponse)&&($requestHandler->dataResponse == 'success')){
                        foreach($requestHandler->reports as $row){
                            
                            echo "<tr class='openForEdit' num='".$row['id']."'>";
                                echo "<td>".$row['agentId']."</td>";
                                echo "<td>".$row['totalRow']."</td>";
                                echo "<td>".$row['totalRenew']."</td>";
                                echo "<td>".$row['totalUnRenew']."</td>";
                                echo "<td>".(round((($row['totalRenew']/$row['totalRow'])*100),2))."%</td>";
                            echo "</tr>";
                           
                        }
                    }
                    echo "<td><b>".$language->reports->totalCount."</b></td>";
                    echo "<td><b>".$countTotalRow."</b></td>";
                    echo "<td><b>".$countTotalRenew."</b></td>";
                    echo "<td><b>".$countTotalUnRenew."</b></td>";
                    echo "<td><b>".(round((($countTotalRenew/$countTotalRow)*100),2))."%</b></td>";
                echo "</tbody>";
            echo "</table>";
        }
        else if ($_REQUEST['reportType']=='report2'){
            echo "<table class='searchReportTab'>";
                echo "<thead>";
                    echo "<tr>";
                    echo "<td>".$language->reports->agentId."</td>";
                    echo "<td>".$language->reports->totalRow."</td>";
                    echo "<td>".$language->reports->totalUnRenew."</td>";
                    echo "<td>".$language->reports->none."</td>";
                    echo "<td>".$language->reports->cancel."</td>";
                    echo "<td>".$language->reports->monthlyOther."</td>";
                    echo "</tr>";
                    echo "</thead>";
                    
                    echo "<tbody>";
                    if (!$requestHandler->reports){
                        echo "<tr ><td colspan='8'><div id='noDataMessage'>".$language->general->noData."</div><td></tr>";
                    }
                    if (isset($requestHandler->dataResponse)&&($requestHandler->dataResponse == 'success')){
                        foreach($requestHandler->reports as $row){
                            
                            echo "<tr class='openForEdit' num='".$row['id']."'>";
                                echo "<td>".$row['agentId']."</td>";
                                echo "<td>".$row['totalRow']."</td>";
                                echo "<td>".$row['totalUnRenew']."</td>";
                                echo "<td>".$row['none']."</td>";
                                echo "<td>".$row['cancel']."</td>";
                                echo "<td>".$row['monthlyOther']."</td>";
                            echo "</tr>";
                           
                        }
                    }
                    echo "<td><b>".$language->reports->totalCount."</b></td>";
                    echo "<td><b>".$countTotalRow."</b></td>";
                    echo "<td><b>".$countTotalUnRenew."</b></td>";
                    echo "<td><b>".$countNone."</b></td>";
                    echo "<td><b>".$countCancel."</b></td>";
                    echo "<td><b>".$countMonthlyOther."</b></td>";
                echo "</tbody>";
            echo "</table>";
        }
            
        ?>
    </div>
</body>
</html>