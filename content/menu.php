<div class='helloDiv'><?php echo "".$language->menu->hello." ".$_SESSION['userInfo']['firstName']." ".$_SESSION['userInfo']['lastName']."" ?></div>
<div class='errorRes'><?php if (isset($requestHandler->dataResponse))
            echo "Status : ".$requestHandler->dataResponse; ?></div>
<div class = 'langSelectBox'>
    <?php
    
    $langFormEng = new inputBox;
    $langFormEng ->setId('langFormEng');
    $langFormEng ->setType('button');
    $langFormEng ->setValue('Eng');
    echo $langFormEng->createInput();
    
    $langFormHeb = new inputBox;
    $langFormHeb ->setId('langFormHeb');
    $langFormHeb ->setType('button');
    $langFormHeb ->setValue('���');
    echo $langFormHeb->createInput();
    ?>
</div>
<form id='updateLingo'>
    <input type='hidden' id='lingoVal' name='lang'>
    <input type='hidden' name='formAction' value='changeLingo' />
</form>
<table class='mainMenu'>
    <tr>
        <td class='menuItem' onclick='document.location = "search.php"'><?php echo $language->menu->search ?></td>
        <?php if($_SESSION['userInfo']['superUser'] == 'yes'){ ?>
            <td class='menuItem' onclick='document.location = "admin.php"'><?php echo $language->menu->admin ?></td>
        <?php } ?>
        <td class='menuItem' onclick='document.location = "exit.php"'><?php echo $language->menu->Exit ?></td>
    </tr>
</table>