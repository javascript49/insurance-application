<?php require_once('session.php'); ?>
<body onload='javascriptPages.mainReportsMin();'>
    <?php require_once('menu.php'); ?>
    <div class='mainCont'>
        <?php
        
        $reportForm = new form;
        $reportForm->setForm($formArray->getMinutesReportsForm());
        $reportForm->setName('minutesReport');
        $reportForm->setMethod('POST');
        $reportForm->setTitle($language->reports->minutesReport);
        echo $reportForm->createForm();
        
        ?>
    </div>
</body>
</html>