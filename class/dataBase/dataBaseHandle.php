<?php
class dataBaseHandle {
    
    /*Private var's*/
    private  $dbHost;
    private  $dbName;
    private  $dbUser;
    private  $dbPass;
    private  $db;
    
    /*Setters*/
    public function setDbHost($host){
        $this->dbHost = $host;
    }
    
    public function setDbName($name){
        $this->dbName = $name;
    }
    
    public function setDbUser($user){
        $this->dbUser = $user;
    }
    
    public function setDbPass($pass){
        $this->dbPass = $pass;
    }
    
    /*Getters*/
    public function getDbHost(){
        return $this->dbHost;
    }
    
    public function getDbName(){
        return $this->dbName;
    }
    
    public function getDbUser(){
        return $this->dbUser;
    }
    
    public function getDbPass(){
        return $this->dbPass;
    }
    
    /*Create a DataBase connection*/
    public function dbConnect(){
        try {
          $this->db = new PDO("mysql:host=".$this->dbHost.";dbname=".$this->dbName.";charset=HEBREW", $this->dbUser, $this->dbPass);//,array(PDO::MYSQL_ATTR_INIT_COMMAND =>"SET NAMES utf8")
          return $this->db;
        }
        catch(PDOException $e) {
            echo $e->getMessage();
            return false;
        }
    }
    
    public function createDataBase(){

        /* dataBase structure for table `agent` */
        try {
            $this->db = new PDO("mysql:host=".$this->dbHost.";charset=HEBREW", $this->dbUser, $this->dbPass);
            $this->db->query("CREATE DATABASE `".$this->dbName."` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci; USE `".$this->dbName."`;");
            echo 'DataBase '.$this->dbName.' successfully created.<br>';
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function createTables(){
        
        /* Table structure for table `agent` */
        try {
            $this->db->query(
            "CREATE TABLE IF NOT EXISTS `agent` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `firstName` varchar(50) NOT NULL,
                `lastName` varchar(50) NOT NULL,
                `email` varchar(150) NOT NULL,
                `phonePre` int(11) NOT NULL,
                `phoneNum` int(11) NOT NULL,
                `userName` varchar(100) NOT NULL,
                `password` varchar(512) NOT NULL,
                `lang` enum('heb','eng') NOT NULL DEFAULT 'eng',
                `status` enum('active','disabled') NOT NULL DEFAULT 'active',
                `superUser` enum('yes','no') NOT NULL DEFAULT 'no',
                PRIMARY KEY (`id`),
                UNIQUE KEY `userName` (`userName`),
                KEY `id` (`id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
            echo 'Table agent successfully created.<br>';
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        
        /* Table structure for table `subscription` */
        try {
            $this->db->query(
            "CREATE TABLE IF NOT EXISTS `record` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `agentId` varchar(100) NOT NULL DEFAULT 'None',
                `insNum` varchar(100) NOT NULL DEFAULT 'None',
                `indicator` varchar(100) NOT NULL DEFAULT 'None',
                `insName` varchar(150) NOT NULL DEFAULT 'None',
                `insId` varchar(100) NOT NULL DEFAULT 'None',
                `sDate` varchar(100) NOT NULL DEFAULT 'None',
                `eDate` varchar(100) NOT NULL DEFAULT 'None',
                `licenseNum` varchar(100) NOT NULL DEFAULT 'None',
                `relate` varchar(1000) NOT NULL DEFAULT 'None',
                `insComp` varchar(100) NOT NULL DEFAULT 'None',
                `status` enum('monthly','monthlyOther','cancel','None') NOT NULL DEFAULT 'None',
                `comments` enum('sold','totaloss','liability','unemployed','None') NOT NULL DEFAULT 'None',
                `handlerName` varchar(150) NOT NULL DEFAULT 'None',
                `open` int(11) NOT NULL,
                `openName` varchar(100) NOT NULL DEFAULT 'None',
                `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `commentsFree` text NOT NULL,
                `exDate` varchar(150) NOT NULL,
                PRIMARY KEY (`id`),
                KEY `openName` (`openName`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
                          echo 'Table record successfully created.<br>';
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        echo 'All Tables were created :) <br>';       
        
    }
    function createAdmin($request){
        try {
            $this->db->query(
            "INSERT INTO  `agent` (`phonePre`,`phoneNum`,`firstName`,`lastName`,`email`,`userName`,`password`,`superUser`)
                            VALUES ('".$request['phonePre']."','".$request['phoneNum']."','".$request['firstName']."','".$request['lastName']."','".$request['email']."','".$request['userName']."','".$request['password']."','yes')");
            echo 'Create Admin account on userName : admin password : 123456<br>';
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
        echo 'Please DELETE the Install.php file !<br>Goodbye';
    }
} 
?>