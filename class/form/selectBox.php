<?php
class selectBox extends inputBox{
    
    /*Private var's*/
    private $option;
    
    /* Setters */
    public function setNewOption($value,$label){
        $this->option.= '<option value="'.$value.'">'.$label.'</option>';
    }
    
    /* general functions */

    public function createInput(){
        
        if (isset($this->Class))    
            $select = '<div class="'.$this->Class.'" >';
        else
            $select = '';
        
        if (isset($this->label))
            $select .= '<label>'.$this->label.'</label>';
            
        $select .= '<select ';
            
        if (isset($this->name))
            $select.= ' name = "'.$this->name.'" ';
        if (isset($this->id))
            $select.= ' id = "'.$this->id.'" ';
        if (isset($this->value))
            $select.= ' value = "'.$this->value.'" ';
            
        $select.= ' >';
        
        $select.= $this->option;
        
        $select.= ' </select> ';
        
        if (isset($this->Class))    
            $select.= '</div>' ; 
        
        return $select;
    }
}

?>