<?php
class form {
    /*Private var's*/
    private $form;
    private $result;
    private $action;
    private $id;
    private $name;
    private $method;
    private $target;
    private $title;
    private $className;
    private $idName;
    private $hideFormTag=false;
    private $tableClass;
    private $rowNum;
    
    /* Setters */
    public function setForm($array){
        $this->form = $array;
    }
    public function setFormClass($value){
        $this->className = $value;
    }
    public function setFormId($value){
        $this->idName = $value;
    }
    public function setTableClass($value){
        $this->tableClass = $value;
    }
    public function setAction($value){
        $this->action = $value;
    }
    public function setHideFormTag($value){
        $this->hideFormTag = $value;
    }
    public function setTitle($value){
        $this->title = $value;
    }
    public function setId($value){
        $this->id = $value;
    }
    public function setName($value){
        $this->name = $value;
    }
    public function setMethod($value){
        $this->method = $value;
    }
    public function setTarget($value){
        $this->target = $value;
    }
    public function setRowNum($value){
        $this->rowNum = $value;
    }
    
    /* Getters */
    public function getForm(){
        return $this->form;
    }
    public function getTableClass(){
        return $this->tableClass;
    }
    public function getFormId(){
        return $this->idName;
    }
    public function getFormClass(){
        return $this->className;
    }
    public function getHideFormTag(){
        return $this->hideFormTag;
    }
    public function getAction(){
        return $this->action;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getMethod(){
        return $this->method;
    }
    public function getTarget(){
        return $this->target;
    }
    public function getRowNum(){
        return $this->rowNum ;
    }
    
    /* general functions */
    public function createForm(){
        
        if($this->hideFormTag == true )
            $this->result = '';
        else{    
            $this->result = "<form ";
            if (isset($this->action))
                $this->result.= ' action = "'.$this->action.'" ';
            if (isset($this->id))
                $this->result.= ' id = "'.$this->id.'" ';
            if (isset($this->name))
                $this->result.= ' name = "'.$this->name.'" ';
            if (isset($this->method))
                $this->result.= ' method = "'.$this->method.'" ';
            if (isset($this->target))
                $this->result.= ' target = "'.$this->target.'" ';
            if (isset($this->idName))
                $this->result.= ' id = "'.$this->idName.'" ';
            if (isset($this->className))
                $this->result.= ' class = "'.$this->className.'" ';
            $this->result.= ' > ';
        }
        $this->result.= ' <table  ';
        if (isset($this->tableClass))
                $this->result.= ' class = "'.$this->tableClass.'" ';
        $this->result.= ' >  ';
        $this->result.= ' <tr><td><h3>'.$this->title.'</h3></td></tr>  ';
        $rowNumber = 0;
        if (isset($this->rowNum)){
            $totalObj = count($this->form);
            $objPerRow = floor($totalObj/$this->rowNum);
        }
        else{
            $objPerRow=1;
        }
        foreach($this->form as $object){
            if (($rowNumber%$objPerRow == 0)){
                $this->result.= '<tr>';
            }
            $this->result.= '<td> ';
            if (isset($object['label']))
                $this->result.= $object['label'] ;
            $this->result.= '</td> <td>';
                
            if ((isset($object['options']))&&($object['type'] == 'select')){
                $input = new selectBox;
                foreach ($object['options'] as $key=>$option){
                    $input->setNewOption($key,$option);
                }
            }
            else if ((isset($object['type']))&&($object['type'] == 'phone')){
                $select = new selectBox;
                foreach ($object['select']['options'] as $key=>$option){
                    $select->setNewOption($key,$option);
                }
                if (isset($object['select']['name']))
                    $select->setName ( $object['select']['name'] );
                if (isset($object['select']['id']))
                    $select->setId ( $object['select']['id'] );
                if (isset($object['select']['value']))
                    $select->setValue ( $object['select']['value'] );
                $this->result .= $select->createInput();
                
                $input=new inputBox;
                if (isset($object['input']['type']))
                    $input->setType ( $object['input']['type'] );
                $object = $object['input'];
            }
            else if ((isset($object['type']))&&($object['type'] == 'datePicker')){
                
                $month = new selectBox;
                foreach ($object['month']['options'] as $key=>$option){
                    $month->setNewOption($key,$option);
                }
                if (isset($object['month']['name']))
                    $month->setName ( $object['month']['name'] );
                if (isset($object['month']['id']))
                    $month->setId ( $object['month']['id'] );
                if (isset($object['month']['value']))
                    $month->setValue ( $object['month']['value'] );
                if (isset($object['month']['readOnly']))
                    $month->setReadOnly ( $object['month']['readOnly'] );
                if (isset($object['month']['disabled']))
                    $month->setDisabled ( $object['month']['disabled'] );
                $this->result .= $month->createInput();
                
                $year = new selectBox;
                foreach ($object['year']['options'] as $key=>$option){
                    $year->setNewOption($key,$option);
                }
                if (isset($object['year']['name']))
                    $year->setName ( $object['year']['name'] );
                if (isset($object['year']['id']))
                    $year->setId ( $object['year']['id'] );
                if (isset($object['year']['value']))
                    $year->setValue ( $object['year']['value'] );
                if (isset($object['year']['readOnly']))
                    $year->setReadOnly ( $object['year']['readOnly'] );
                if (isset($object['year']['disabled']))
                    $year->setDisabled ( $object['year']['disabled'] );
                $this->result .= $year->createInput();
                
                $input=new inputBox;
                $input->setType('hidden');
            }
            else if ((isset($object['type']))&&($object['type'] == 'search')){
                $search = new inputBox;
                if (isset($object['search']['name']))
                    $search->setName ( $object['search']['name'] );
                if (isset($object['search']['id']))
                    $search->setId ( $object['search']['id'] );
                if (isset($object['search']['value']))
                    $search->setValue ( $object['search']['value'] );
                if (isset($object['search']['placeHolder']))
                    $search->setPlaceHolder ( $object['search']['placeHolder'] );
                $this->result .= $search->createInput();
                
                $input=new inputBox;
                if (isset($object['input']['type']))
                    $input->setType ( $object['input']['type'] );
                $object = $object['input'];
            }
            else if ((isset($object['type']))&&($object['type'] == 'textarea')){
                $input = new textArea;
                if (isset($object['rows']))
                    $input->setRows ( $object['rows'] );
                if (isset($object['cols']))
                    $input->setCols ( $object['cols'] );
                if (isset($object['maxLength']))
                    $input->setMaxLength ( $object['maxLength'] );
            }
            else {
                $input=new inputBox;
                if (isset($object['type']))
                    $input->setType ( $object['type'] );
            }
            if (isset($object['readOnly']))
                $input->setReadOnly ( $object['readOnly'] );
            if (isset($object['class']))
                $input->setClass ( $object['class'] );
            if (isset($object['pattern']))
                $input->setPattern ( $object['pattern'] );
            if (isset($object['required']))
                $input->setRequired ( $object['required'] );   
            if (isset($object['min']))
                $input->setMin ( $object['min'] );
            if (isset($object['max']))
                $input->setMax ( $object['max'] );
            if (isset($object['name']))
                $input->setName ( $object['name'] );
            if (isset($object['id']))
                $input->setId ( $object['id'] );
            if (isset($object['value']))
                $input->setValue ( $object['value'] );
            if (isset($object['disabled']))
                $input->setDisabled ( $object['disabled'] );
            if (isset($object['placeHolder']))
                $input->setPlaceHolder ( $object['placeHolder'] );
                
            $this->result .= $input->createInput();
            $this->result.= ' </td>';
            $rowNumber++;
            if (($rowNumber%$objPerRow==0)){
                $this->result.= '</tr>';
            }
        }
        $this->result .= " </table> ";
        if($this->hideFormTag == false )
            $this->result .= " </form> ";
        return $this->result;
    }
}

?>