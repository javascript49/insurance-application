<?php
class uploadFile {
    
    /* Private Vars */
    private $allowedExts;
    private $fileType;
    private $fileSize;
    private $uploadFolder;
    private $fileName;
    
    /* Setters */
    public function setAllowedExts($value){
        $this->allowedExts = $value;
    }
    
    public function setFileType($value){
        $this->fileType = $value;
    }
    
    public function setFileSize($value){
        $this->fileSize = $value;
    }
    
    public function setUploadFolder($value){
        $this->uploadFolder = $value;
    }
    
    public function setFileName($value){
        $this->fileName = $value;
    }
    
    /* Getters */
    public function getAllowedExts(){
        return $this->allowedExts;
    }
    
    public function getFileType(){
        return $this->fileType;
    }
    
    public function getFileSize(){
        return $this->fileSize;
    }
    
    public function getUploadFolder(){
        return $this->uploadFolder;
    }
    
    public function getFileName(){
        return $this->fileName;
    }
    
    /* General Methods */
    public function upload($file){
        
        $tempArray = explode(".", $file["name"]);
        $extension = end($tempArray);
        $fileTypeOk = false;
        foreach ($this->fileType as $fileType){
            if ($file['type'] == $fileType)
                $fileTypeOk = true;
        }
        if (( $fileTypeOk == true )&& ($file["size"] < $this->fileSize)&& in_array($extension, $this->allowedExts)){
            if ($file["error"] > 0)
                return "Return Code: " . $file["error"] ;
            else{
                
                $details = "Upload: " . $this->fileName.".".$extension . "\n
                Type: " . $file["type"] . "\n
                Size: " . ($file["size"] / 1024) . " kB\n
                Temp file: " . $file["tmp_name"] . "\n";
                                
                if (file_exists($this->uploadFolder ."/". $this->fileName.".".$extension))
                    return "Error -> File" .$this->fileName.".".$extension . " already exists. ";
                else{
                    move_uploaded_file($file["tmp_name"],
                    $this->uploadFolder ."/". $this->fileName.".".$extension);
                    return "File uploaded successfully";
                }
            }
        }
        else
            return "Invalid file";
    }
}
?>