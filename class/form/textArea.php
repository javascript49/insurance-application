<?php
class textArea {
    /*Private var's*/
    protected  $placeHolder;
    protected  $rows;
    protected  $cols;
    protected  $readOnly;
    protected  $name;
    protected  $id;
    protected  $value;
    protected  $label;
    protected  $Class;
    protected  $required;
    protected  $pattern;
    protected  $disabled;
    protected  $maxLength;
    
    
    /*Setters*/
    public function setPattern($_value){
        $this->pattern = $_value;
    }
    public function setRequired($_value){
        $this->required = $_value;
    }
    public function setPlaceHolder($_value){
        $this->placeHolder = $_value;
    }
    public function setRows($_value){
        $this->rows = $_value;
    }
    public function setCols($_value){
        $this->cols = $_value;
    }
    public function setReadOnly($_value){
        $this->readOnly = $_value;
    }
    public function setName($_value){
        $this->name = $_value;
    }
    public function setId($_value){
        $this->id = $_value;
    }
    public function setValue($_value){
        $this->value = $_value;
    }
    public function setLabel($_value){
        $this->label = $_value;
    }
    public function setClass($_value){
        $this->Class = $_value;
    }
    public function setDisabled($_value){
        $this->disabled = $_value;
    }
    public function setMaxLength($_value){
        $this->maxLength = $_value;
    }
    
    /*Getters*/
    public function getPattern(){
        return $this->pattern;
    }
    public function getRequired(){
        return $this->required;
    }
    public function getPlaceHolder(){
        return $this->placeHolder;
    }
    public function getRows(){
        return $this->rows;
    }
    public function getCols(){
        return $this->cols;
    }
    public function getReadOnly(){
        return $this->readOnly;
    }
    public function getName(){
        return $this->name;
    }
    public function getId(){
        return $this->id;
    }
    public function getValue(){
        return $this->value;
    }
    public function getLabel(){
        return $this->label;
    }
    public function getClass(){
        return $this->Class;
    }
    public function getDisabled(){
        return $this->disabled;
    }
    public function getMaxLength(){
        return $this->maxLength;
    }
    
    /* general functions */
    public function createInput(){
        
            
        if (isset($this->label))
            $textArea = '<label>'.$this->label.'</label> <textarea ';
        else    
            $textArea = '<textarea ';
        
        if (isset($this->maxLength))
            $textArea.= ' maxlength = "'.$this->maxLength.'" ';
        if (isset($this->Class))
            $textArea.= ' class = "'.$this->Class.'" ';
        if (isset($this->readOnly))
            $textArea.= ' readonly = "'.$this->readOnly.'" ';
        if (isset($this->pattern))
            $textArea.= ' pattern = "'.$this->pattern.'" ';
        if (isset($this->required))
            $textArea.= ' required = "'.$this->required.'" ';
        if (isset($this->rows))
            $textArea.= ' rows = "'.$this->rows.'" ';
        if (isset($this->cols))
            $textArea.= ' cols = "'.$this->cols.'" ';
        if (isset($this->name))
            $textArea.= ' name = "'.$this->name.'" ';
        if (isset($this->id))
            $textArea.= ' id = "'.$this->id.'" ';
        if (isset($this->placeHolder))
            $textArea.= ' placeHolder = "'.$this->placeHolder.'" ';
        if (isset($this->disabled))
            $textArea.= ' disabled = "'.$this->disabled.'" ';
       
        $textArea.= ' > ';
        if (isset($this->value))
            $textArea.= $this->value;
            
        $textArea.= '</textarea>';
        
        return $textArea;
    }
}

?>