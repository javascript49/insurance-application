<?php
class inputBox {
    /*Private var's*/
    protected  $type;
    protected  $placeHolder;
    protected  $min;
    protected  $max;
    protected  $name;
    protected  $id;
    protected  $value;
    protected  $label;
    protected  $Class;
    protected  $required;
    protected  $pattern;
    protected  $disabled;
    protected  $readonly;
    
    
    /*Setters*/
    public function setType($_value){
        $this->type = $_value;
    }
    public function setPattern($_value){
        $this->pattern = $_value;
    }
    public function setRequired($_value){
        $this->required = $_value;
    }
    public function setPlaceHolder($_value){
        $this->placeHolder = $_value;
    }
    public function setMin($_value){
        $this->min = $_value;
    }
    public function setMax($_value){
        $this->max = $_value;
    }
    public function setName($_value){
        $this->name = $_value;
    }
    public function setId($_value){
        $this->id = $_value;
    }
    public function setValue($_value){
        $this->value = $_value;
    }
    public function setLabel($_value){
        $this->label = $_value;
    }
    public function setClass($_value){
        $this->Class = $_value;
    }
    public function setDisabled($_value){
        $this->disabled = $_value;
    }
    public function setReadOnly($_value){
        $this->readOnly = $_value;
    }
    
    /*Getters*/
    public function getType(){
        return $this->type;
    }
    public function getPattern(){
        return $this->pattern;
    }
    public function getRequired(){
        return $this->required;
    }
    public function getPlaceHolder(){
        return $this->placeHolder;
    }
    public function getMin(){
        return $this->min;
    }
    public function getMax(){
        return $this->max;
    }
    public function getName(){
        return $this->name;
    }
    public function getId(){
        return $this->id;
    }
    public function getValue(){
        return $this->value;
    }
    public function getLabel(){
        return $this->label;
    }
    public function getClass(){
        return $this->Class;
    }
    public function getDisabled(){
        return $this->disabled;
    }
    public function getReadOnly(){
        return $this->readOnly;
    }
    
    /* general functions */
    public function createInput(){
        
        if (isset($this->Class))
            $input = '<div class="'.$this->Class.'" >';
        else
            $input = '';
            
        if (isset($this->label))
            $input .= '<label>'.$this->label.'</label> <input ';
            
        $input .= '<input ';
            
        if (isset($this->type))
            $input.= ' type = "'.$this->type.'" ';
        if (isset($this->pattern))
            $input.= ' pattern = "'.$this->pattern.'" ';
        if (isset($this->required))
            $input.= ' required = "'.$this->required.'" ';
        if (isset($this->min))
            $input.= ' min = "'.$this->min.'" ';
        if (isset($this->max))
            $input.= ' max = "'.$this->max.'" ';
        if (isset($this->name))
            $input.= ' name = "'.$this->name.'" ';
        if (isset($this->id))
            $input.= ' id = "'.$this->id.'" ';
        if (isset($this->value))
            $input.= ' value = "'.$this->value.'" ';
        if (isset($this->placeHolder))
            $input.= ' placeHolder = "'.$this->placeHolder.'" ';
        if (isset($this->disabled))
            $input.= ' disabled = "'.$this->disabled.'" ';
        if (isset($this->readOnly))
            $input.= ' readonly = "'.$this->readOnly.'" ';
       
        $input.= ' > ';
        
        if (isset($this->Class))    
            $input.= '</div>';

        return $input;
    }
}

?>