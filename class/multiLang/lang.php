<?php
class lang {
    
    /*Private var's*/
    private static $language;
    
    /*Setters*/
    public function setLang($_language){
        self::$language = $_language;
    }
    
    /*Getters*/
    public function getLang(){
        return self::$language;
    }
    
    public function matchSimilar($_value){
        $lang = self::getLang();
        if ($lang == 'eng')
            $findSim = 'heb';
        else
            $findSim = 'eng';
            
        foreach (self::createArray() as $value){
            foreach ($value as $phrase)
                if($phrase[$findSim]==$_value)
                    return $phrase[$lang];
        }
    }
    
    public function enumTranslation($_value){
            
        foreach (self::createArray() as $value){
            foreach ($value as $key=>$phrase){
                if($key == $_value){
                    return $phrase['heb'];
                }
            }
        }
    }
    
    private function createArray(){
        $dictionary =  new stdClass;
        
        $page = $dictionary->login = new stdClass; //set current page language
        $page->login        = Array('eng'=>'login',
                                    'heb'=>'�����');
        $page->header       = Array('eng'=>'Agent Track',
                                    'heb'=>'���� ������');
        $page->error       = Array('eng'=>'Error -> one of the details is incorrect!',
                                    'heb'=>'����� -> ��� ������ ���� ����!');
        

        $page = $dictionary->menu  = new stdClass;
        $page->hello        = Array('eng'=>'Hello',
                                    'heb'=>'����');
        $page->subscription = Array('eng'=>'Add Subscription',
                                    'heb'=>'���� ����');
        $page->payment      = Array('eng'=>'Enter Payment Details',
                                    'heb'=>'���� ���� �����');
        $page->package      = Array('eng'=>'Choose Package',
                                    'heb'=>'��� �����');
        $page->operation    = Array('eng'=>'Main Admin Operation',
                                    'heb'=>'������ ���� �����');
        $page->admin        = Array('eng'=>'Admin Area',
                                    'heb'=>'���� ����');
        $page->upload       = Array('eng'=>'Upload Excel File',
                                    'heb'=>'����� ���� ����');
        $page->search       = Array('eng'=>'Data Search',
                                    'heb'=>'����� ������');
        $page->Exit         = Array('eng'=>'Exit',
                                    'heb'=>'�����');
        
        $page = $dictionary->mainForm  = new stdClass;
        $page->ind1         = Array('eng'=>'1',
                                    'heb'=>'1 -> ����');
        $page->ind2         = Array('eng'=>'2',
                                    'heb'=>'2 -> �� �');
        $page->ind3         = Array('eng'=>'3',
                                    'heb'=>'3 -> ���� ����');
        $page->agentId      = Array('eng'=>'Agent Id',
                                    'heb'=>'���� ����');
        $page->insNum       = Array('eng'=>'Insurance Number',
                                    'heb'=>'���� ������');
        $page->indicator    = Array('eng'=>'Indicator',
                                    'heb'=>'���������');
        $page->insName      = Array('eng'=>'Insurance Owner Name',
                                    'heb'=>'�� ������');
        $page->insId        = Array('eng'=>'Insurance Owner Id',
                                    'heb'=>'���� ���� ������');
        $page->sDate        = Array('eng'=>'Start Date',
                                    'heb'=>'����� �����');
        $page->eDate        = Array('eng'=>'End Date',
                                    'heb'=>'����� ����');
        $page->exDate       = Array('eng'=>'Excel Date',
                                    'heb'=>'���� �����');
        $page->licenseNum   = Array('eng'=>'license Number',
                                    'heb'=>'���� ���');
        $page->relate       = Array('eng'=>'Relation',
                                    'heb'=>'���� �������');
        $page->employee     = Array('eng'=>'HH"I Employee',
                                    'heb'=>'���� ��"�');
        $page->family       = Array('eng'=>'Family',
                                    'heb'=>'�����');
        $page->retired      = Array('eng'=>'Retired',
                                    'heb'=>'�����');
        $page->other        = Array('eng'=>'Other',
                                    'heb'=>'���');
        $page->ayalon       = Array('eng'=>'Ayalon',
                                    'heb'=>'������');
        $page->harel        = Array('eng'=>'Harel',
                                    'heb'=>'����');
        $page->shomera      = Array('eng'=>'Shomera',
                                    'heb'=>'�����');
        $page->fenix        = Array('eng'=>'Fenix',
                                    'heb'=>'������');
        $page->menora       = Array('eng'=>'Menora',
                                    'heb'=>'�����');
        $page->migdal       = Array('eng'=>'Migdal',
                                    'heb'=>'����');
        $page->hachshara    = Array('eng'=>'Hachshara',
                                    'heb'=>'�����');
        $page->clal         = Array('eng'=>'Clal',
                                    'heb'=>'���');
        $page->insComp      = Array('eng'=>'Insurance Company',
                                    'heb'=>'���� �����');
        $page->status       = Array('eng'=>'Status',
                                    'heb'=>'�����');
        $page->monthly      = Array('eng'=>'renew',
                                    'heb'=>'����');
        $page->monthlyOther = Array('eng'=>'Renew In Other Company',
                                    'heb'=>'���� ����� ����');
        $page->cancel       = Array('eng'=>'Cancel',
                                    'heb'=>'�����');
        $page->sold         = Array('eng'=>'Sold Car',
                                    'heb'=>'��� ����');
        $page->totaloss     = Array('eng'=>'Totaloss',
                                    'heb'=>'�������');
        $page->liability    = Array('eng'=>'Independent Liability Insurance',
                                    'heb'=>'����� ���� �����');
        $page->unemployed   = Array('eng'=>'Unemployed',
                                    'heb'=>'��� �����');
        $page->comments     = Array('eng'=>'Comments',
                                    'heb'=>'�����');
        $page->commentsFree = Array('eng'=>'Free Text Comments',
                                    'heb'=>'����� ���� �����');
        $page->handlerName  = Array('eng'=>'Updated By',
                                    'heb'=>'����� �"�');
        $page->mainForm     = Array('eng'=>'Main Form',
                                    'heb'=>'���� ����');
        $page->open         = Array('eng'=>'File Is Open',
                                    'heb'=>'���� ����');
        $page->openName     = Array('eng'=>'Opener Name',
                                    'heb'=>'�� ���� �����');
        $page->date         = Array('eng'=>'Date Of Insert',
                                    'heb'=>'����� �����');
        $page->id           = Array('eng'=>'ID Number',
                                    'heb'=>'���� ������');
        $page->none         = Array('eng'=>'None',
                                    'heb'=>'���');
        
        $page = $dictionary->dealerOperation  = new stdClass;
        $page->menuAdd      = Array('eng'=>'Add User',
                                    'heb'=>'���� �����');
        $page->menuEdit     = Array('eng'=>'Edit User',
                                    'heb'=>'���� �����');
        $page->menuBlock    = Array('eng'=>'Block User',
                                    'heb'=>'���� �����');
        $page->menuOpen     = Array('eng'=>'UnBlock User',
                                    'heb'=>'���� �����');
        $page->chooseAgent  = Array('eng'=>'Choose User To Block',
                                    'heb'=>'��� ����� �����');
        $page->editUser     = Array('eng'=>'Choose User To Edit',
                                    'heb'=>'��� ����� �����');
        $page->openAgent    = Array('eng'=>'Choose User To UnBlock',
                                    'heb'=>'��� ����� ����� ������');
        $page->menuMove     = Array('eng'=>'Move Phone Numbers From one Agent to Another',
                                    'heb'=>'���� ����� ����� ��� ������');
        $page->agentMoveTo  = Array('eng'=>'Choose Agent that You want to pass him/her all the phones',
                                    'heb'=>'��� ���� ����� ������ �� ����� ������ ����');
        $page->menuAddCsv   = Array('eng'=>'Add New Subscription from Csv File',
                                    'heb'=>'���� ���� ����� CSV');
        $page->chooseOp     = Array('eng'=>'Choose Operation',
                                    'heb'=>'��� �����');
        $page->agentMove    = Array('eng'=>'Choose Agent that You want to move his/her phones',
                                    'heb'=>'��� ���� ����� ������ �� ����� ������ ���\���');
        $page->howToCsv     = Array('eng'=>'How To Create Csv File',
                                    'heb'=>'��� ����� ���� CSV');
        $page->moveCli      = Array('eng'=>'Move selected Cli',
                                    'heb'=>'���� ������ ������');
        $page->uploadFile   = Array('eng'=>'Upload Excel File',
                                    'heb'=>'����� ���� ����');
        $page->searchRec    = Array('eng'=>'Search For Record',
                                    'heb'=>'����� ��� �����');
        $page->addUser      = Array('eng'=>'Add User',
                                    'heb'=>'���� �����');
        $page->blockUser    = Array('eng'=>'Block User',
                                    'heb'=>'���� �����');
        $page->openUser     = Array('eng'=>'UnBlock User',
                                    'heb'=>'���� �����');
        $page->reports      = Array('eng'=>'Reports',
                                    'heb'=>'��"���');
        $page->export       = Array('eng'=>'Data Export',
                                    'heb'=>'����� ������');
        
        
        $page = $dictionary->reports  = new stdClass;
        $page->renew        = Array('eng'=>'First Report -> Renew',
                                    'heb'=>'��"� ����� -> ��"� �����');
        $page->unrenew      = Array('eng'=>'Second Report -> UnRenew',
                                    'heb'=>'��"� ��� -> ��"� �� �����');
        $page->submitReport = Array('eng'=>'Show Report',
                                    'heb'=>'��� ���');
        $page->choiceOp     = Array('eng'=>'Choose Report',
                                    'heb'=>'��� ��"�');
        $page->agentId      = Array('eng'=>'Agent Id',
                                    'heb'=>'���� ����');
        $page->totalRow     = Array('eng'=>'Total Records',
                                    'heb'=>'��"� ������');
        $page->totalRenew   = Array('eng'=>'Total ReNew',
                                    'heb'=>'��"� �����');
        $page->totalUnRenew = Array('eng'=>'Total UnReNew',
                                    'heb'=>'��"� �� �����');
        $page->none         = Array('eng'=>'Status UnDefined',
                                    'heb'=>'����� �� �����');
        $page->cancel       = Array('eng'=>'Canceled',
                                    'heb'=>'�����');
        $page->monthlyOther = Array('eng'=>'ReNew In Another',
                                    'heb'=>'����� ������� ����');
        $page->reportsHeader= Array('eng'=>'Recored For Month',
                                    'heb'=>'������ �����');
        $page->percent      = Array('eng'=>'ReNew Percent',
                                    'heb'=>'���� �����');
        $page->printRep     = Array('eng'=>'Print Report',
                                    'heb'=>'����� ���');
        $page->difference   = Array('eng'=>'Difference',
                                    'heb'=>'����');
        $page->totalCount  = Array('eng'=>'Total Count',
                                    'heb'=>'��"�');
        
        $page = $dictionary->general  = new stdClass;
        $page->forceFileOpen= Array('eng'=>'Force File Open',
                                    'heb'=>'���� ����� ����');
        $page->noData       = Array('eng'=>'There is no data for display',
                                    'heb'=>'��� ������ �����');
        $page->chooseMenu   = Array('eng'=>'choose....',
                                    'heb'=>'���....');
        $page->areYouSure   = Array('eng'=>'Are You Sure You Want To Edit This Reecord?',
                                    'heb'=>'��� ��� ���� ������� ����� ����� ���?');
        $page->firstName    = Array('eng'=>'First Name',
                                    'heb'=>'�� ����');
        $page->lastName     = Array('eng'=>'Last Name',
                                    'heb'=>'�� �����');
        $page->email        = Array('eng'=>'Email',
                                    'heb'=>'���� ��������');
        $page->phone        = Array('eng'=>'Phone Number:',
                                    'heb'=>'���� �����');
        $page->username     = Array('eng'=>'username',
                                    'heb'=>'�� �����');
        $page->password     = Array('eng'=>'password',
                                    'heb'=>'�����');
        $page->search       = Array('eng'=>'search',
                                    'heb'=>'���');
        $page->chooseReport = Array('eng'=>'Choose Report',
                                    'heb'=>'��� �����');
        $page->selectView   = Array('eng'=>'Select View',
                                    'heb'=>'��� �����');
        $page->showReport   = Array('eng'=>'Show Report',
                                    'heb'=>'���� �����');
        $page->excelExp     = Array('eng'=>'Export to excel',
                                    'heb'=>'���� ����� EXCEL');
        $page->fromDate     = Array('eng'=>'From Date',
                                    'heb'=>'������');
        $page->toDate       = Array('eng'=>'To Date',
                                    'heb'=>'������');
        $page->direction    = Array('eng'=>'ltr',
                                    'heb'=>'rtl');
        $page->floatDir     = Array('eng'=>'left',
                                    'heb'=>'right');
        $page->submit       = Array('eng'=>'submit form',
                                    'heb'=>'��� ����');
        $page->update       = Array('eng'=>'Update Data',
                                    'heb'=>'���� �����');
        $page->uploadFile   = Array('eng'=>'Upload Excel File',
                                    'heb'=>'����� ���� ����');
        $page->chooseFile   = Array('eng'=>'Choose File To Upload',
                                    'heb'=>'��� ���� ������');
        $page->yes          = Array('eng'=>'Yes',
                                    'heb'=>'��');
        $page->no           = Array('eng'=>'No',
                                    'heb'=>'��');
        $page->closeEdit    = Array('eng'=>'Close Edit Window?',
                                    'heb'=>'����� ���� �����?');
        $page->openRedit    = Array('eng'=>'Force open file will cancel other users work on that file, Are you sure?',
                                    'heb'=>'���� ����� ����� ���� �� ����� �������� ������ �� �����, ��� ��� ����?');
        $page->openAll      = Array('eng'=>'Open All Fields For Edit',
                                    'heb'=>'��� �� �� ����� ������');
        $page->submitExport = Array('eng'=>'Export To Excel',
                                    'heb'=>'���� ����� ����');
        $page->dateAlert    = Array('eng'=>'End Date Cant be smaller then Start date',
                                    'heb'=>'����� ���� �� ���� ����� ��� ������ �����');
        $page->buttonOne    = Array('eng'=>'creating file..',
                                    'heb'=>'���� ����..');
        $page->buttonSec    = Array('eng'=>'click for download',
                                    'heb'=>'��� ������');
        $page->None         = Array('eng'=>'None',
                                    'heb'=>'���');
        
        $page = $dictionary->edit  = new stdClass;
        $page->editForm     = Array('eng'=>'Edit Details Form',
                                    'heb'=>'���� ����� ������');
        $page->allAgents    = Array('eng'=>'All Agents',
                                    'heb'=>'�� �������');
        $page->countryReport= Array('eng'=>'Report According Country',
                                    'heb'=>'����� �� �� �����');
        $page->rootAgent    = Array('eng'=>'Root Agent',
                                    'heb'=>'���� �����');
        $page->oneAgent     = Array('eng'=>'One Agent',
                                    'heb'=>'���� ���');
        $page->chooseAgent  = Array('eng'=>'Choose Agent Name',
                                    'heb'=>'��� �� ����');
        
        $page = $dictionary->search  = new stdClass;
        $page->searchById   = Array('eng'=>'Search By Id Number',
                                    'heb'=>'����� �� �� ���� ����� ����');
        $page->searchByLic  = Array('eng'=>'Search By Licence Number',
                                    'heb'=>'����� �� �� ���� �����');
        $page->searchByIns  = Array('eng'=>'Search By Insurance Number',
                                    'heb'=>'����� �� �� ���� ������');
        $page->searchSdate  = Array('eng'=>'Search By Start Date',
                                    'heb'=>'����� ��� ����� �����');
        $page->searchEdate  = Array('eng'=>'Search By End Date',
                                    'heb'=>'����� ��� ����� ����');
        $page->submitSearch = Array('eng'=>'Start Search',
                                    'heb'=>'���� �����');
        
        return $dictionary;
    }
    
    public function createMultiLang() {
        
        $dictionary = self::createArray();
        
        foreach ($dictionary as $page)
            foreach($page as &$item)
                $item = $item[self::$language];
            
        return $dictionary;
        
    }
}

?>