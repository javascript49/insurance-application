<?php
class setForms {
    protected $language;
    protected $requestHandler;
    private $month=array();
    private $year=array();
    
    public function __construct(){
        for ($i=1;$i<13;$i++){
            if ($i<10)
                $month['0'.$i]='0'.$i;
            else
                $month[$i]=$i;  
        }
        for ($i=1980;$i<2031;$i++){
            $year[$i]=$i;            
        }
        $this->month = $month;
        $this->year  = $year;
    }
    
    
    public function setLanguage($value){
        $this->language = $value;
    }
    
    public function setRequestHandler($value){
        $this->requestHandler = $value;
    }
    
    public function getMainForm(){
        $data = $this->requestHandler->getRecordData();
        return Array(
            0 => Array(
                'type'  => 'number',
                'name' => 'openId',
                'id'   => 'openId',
                'readOnly' => 'readonly',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->id,
                'value' => isset($data['id'])?$data['id']:''
            ),
            1 => Array(
                'type'  => 'number',
                'name' => 'insNum',
                'id'   => 'insNum',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->insNum,
                'value' => isset($data['insNum'])?$data['insNum']:''
            ),
            2 => Array(
                'type'  => 'number',
                'name' => 'agentId',
                'id'   => 'agentId',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->agentId,
                'value' => isset($data['agentId'])?$data['agentId']:''
            ),
            3 => Array(
                'type'  => 'select',
                'options' => Array(
                    '1'  => $this->language->mainForm->ind1,
                    '2'  => $this->language->mainForm->ind2,
                    '3'  => $this->language->mainForm->ind3 
                ),
                'name' => 'indicator',
                'id'   => 'indicator',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->indicator,
                'value' => isset($data['indicator'])?$data['indicator']:''
            ),
            4 => Array(
                'type'  => 'text',
                'name' => 'insName',
                'id'   => 'insName',
                'disabled' => 'disabled',
                'pattern' => '^[\w\u0590-\u05FF][^0-9*]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->insName,
                'value' => isset($data['insName'])?$data['insName']:''
            ),
            5 => Array(
                'type'  => 'number',
                'name' => 'insId',
                'id'   => 'insId',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->insId,
                'value' => isset($data['insId'])?$data['insId']:''
            ),
            6 => Array(
                'type'  => 'number',
                'name' => 'sDate',
                'id'   => 'sDate',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->sDate,
                'value' => isset($data['sDate'])?$data['sDate']:''
            ),
            7 => Array(
                'type'  => 'number',
                'name' => 'eDate',
                'id'   => 'eDate',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->eDate,
                'value' => isset($data['eDate'])?$data['eDate']:''
            ),
            8 => Array(
                'type'  => 'number',
                'name' => 'licenseNum',
                'id'   => 'licenseNum',
                'disabled' => 'disabled',
                'pattern' => '^[0-9]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->licenseNum,
                'value' => isset($data['licenseNum'])?$data['licenseNum']:''
            ),
            9 => Array(
                'type'  => 'select',
                'options' => Array(
                    '���� ��"�' =>$this->language->mainForm->employee,
                    '�����'=>$this->language->mainForm->family,
                    '�����'=>$this->language->mainForm->retired,
                    '���'=>$this->language->mainForm->other
                ),
                'name' => 'relate',
                'id'   => 'relate',
                'disabled' => 'disabled',
                'label' => $this->language->mainForm->relate,
                'value' => isset($data['relate'])?$data['relate']:''
            ),
            10 => Array(
                'type'  => 'select',
                'options' => Array (
                    '������' => $this->language->mainForm->ayalon,
                    '����' => $this->language->mainForm->harel,
                    '�����' => $this->language->mainForm->shomera,
                    '������' => $this->language->mainForm->fenix,
                    '�����' => $this->language->mainForm->menora,
                    '����' => $this->language->mainForm->migdal,
                    '�����' => $this->language->mainForm->hachshara,
                    '���' => $this->language->mainForm->clal
                    
                ),
                'name' => 'insComp',
                'id'   => 'insComp',
                'disabled' => 'disabled',
                'label' => $this->language->mainForm->insComp,
                'value' => isset($data['insComp'])?$data['insComp']:''
            ),
            11 => Array(
                'type'  => 'select',
                'options' => Array(
                    'None' => $this->language->mainForm->none,
                    'monthly' => $this->language->mainForm->monthly,
                    'monthlyOther' => $this->language->mainForm->monthlyOther,
                    'cancel' => $this->language->mainForm->cancel
                ),
                'name' => 'status',
                'id'   => 'status',
                'label' => $this->language->mainForm->status,
                'value' => isset($data['status'])?$data['status']:''
            ),
            12 => Array(
                'type'  => 'select',
                'options' => Array(
                    'None' => $this->language->mainForm->none,
                    'sold' => $this->language->mainForm->sold,
                    'totaloss' => $this->language->mainForm->totaloss,
                    'liability' => $this->language->mainForm->liability,
                    'unemployed' => $this->language->mainForm->unemployed
                ),
                'name' => 'comments',
                'id'   => 'comments',
                'label' => $this->language->mainForm->comments,
                'value' => isset($data['comments'])?$data['comments']:''
            ),
            13 => Array(
                'type'  => 'textarea',
                'name' => 'commentsFree',
                'id'   => 'commentsFree',
                'label' => $this->language->mainForm->commentsFree,
                'value' => isset($data['comments'])?$data['commentsFree']:''
            ),
            14 => Array(
                'type'  => 'text',
                'name' => 'handlerName',
                'id'   => 'handlerName',
                'disabled' => 'disabled',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => $this->language->mainForm->handlerName,
                'value' => isset($data['handlerName'])?$data['handlerName']:''
            ),
            15 => Array(
                'type'  => 'text',
                'name' => 'openField',
                'id'   => 'openField',
                'disabled' => 'disabled',
                'label' => $this->language->mainForm->open,
                'value' => (($data['open'])==0)?$this->language->general->no:$this->language->general->yes
            ),
            16 => Array(
                'type'  => 'text',
                'name' => 'openName',
                'id'   => 'openName',
                'disabled' => 'disabled',
                'label' => $this->language->mainForm->openName,
                'value' => isset($data['openName'])?$data['openName']:''
            ),
            17 => Array(
                'type'  => 'text',
                'name' => 'exDate',
                'id'   => 'exDate',
                'disabled' => 'disabled',
                'label' => $this->language->mainForm->exDate,
                'value' => isset($data['exDate'])?$data['exDate']:''
            ),
            18 => Array(
                'type'  => 'hidden',
                'name' => 'openByElse',
                'id'   => 'openByElse',
                'disabled' => 'disabled',
                'value' => $data['openByElse']
            ),
            19 => Array(
                'type'  => 'hidden',
                'name' => 'formAction',
                'value' => 'editDataForm'
            ),
            20 => Array(
                'type'  => 'button',
                'id'    => 'subData',
                'value' => $this->language->general->update,
                (($data['openByElse'])=='true')?'disabled':'enabled' => (($data['openByElse'])=='true')?'disabled':'enabled'
            )
        );
    }
    
    public function getLoginForm(){
        return Array(
            0 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'userName',
                'required' => 'required',
                'label' => $this->language->general->username
            ),
            1 => Array(
                'type'  => 'password',
                'name' => 'password',
                'id'   => 'password',
                'required' => 'required',
                'label' => $this->language->general->password
            ),
            2 => Array(
                'type'  => 'submit',
                'value' => $this->language->general->submit
            ),
            3 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getLoginForm'
            )
        );
    }
    
    public function getUploadForm(){
        return Array(
            0 => Array(
                'type' => 'file',
                'name' => 'uploadFile',
                'id'   => 'uploadFile',
                'label' => $this->language->general->uploadFile
            ),
            1 => Array(
                'type'  => 'datePicker',
                'month' => Array (
                    'options' => $this->month,
                    'name' => 'monthPick',
                    'value' => date('m'),
                    'id'   => 'monthPick'
                ),
                'year' => Array (
                    'options' => $this->year,
                    'name' => 'yearPick',
                    'value' => date('Y'),
                    'id'   => 'yearPick'
                ),
                'name' => 'exDate',
                'id'   => 'exDate',
                'placeHolder' => '2013-01',
                'required' => 'required',
                'label' => $this->language->mainForm->exDate
            ),
            2 => Array(
                'type' => 'submit',
                'name' => 'formAction',
                'value' => $this->language->general->uploadFile
            ),
            3 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'uploadForm'
            )
        );
    }
    
    public function getAddAgentForm(){
        return Array(
            0 => Array(
                'type'  => 'text',
                'name' => 'firstName',
                'id'   => 'firstName',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => $this->language->general->firstName
            ),
            1 => Array(
                'type'  => 'text',
                'name' => 'lastName',
                'id'   => 'lastName',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => $this->language->general->lastName
            ),
            2 => Array(
                'type'  => 'phone',
                'label' => $this->language->general->phone,
                'select' => Array (
                    'type'  => 'select',
                    'options' => Array(
                        '050' => '050',
                        '052' => '052',
                        '053' => '053',
                        '054' => '054',
                        '055' => '055',
                        '057' => '057',
                        '058' => '058',
                        '05522' => '05522',
                        '05544' => '05544',
                        '05566' => '05566',
                        '05588' => '05588'
                    ),
                    'name' => 'phonePre',
                    'id'   => 'phonePre'
                ),
                'input' => Array(
                    'name' => 'phoneNum',
                    'id'   => 'phonePost',
                    'required' => 'required',
                    'type' => 'number'
                ) 
            ),
            3 => Array(
                'type'  => 'email',
                'pattern' => '[^@]+@[^@]+\.[a-zA-Z]{2,6}',
                'name' => 'email',
                'id'   => 'email',
                'required' => 'required',
                'label' => $this->language->general->email
            ),
            4 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'username',
                'required' => 'required',
                'label' => $this->language->general->username
            ),
            5 => Array(
                'type'  => 'password',
                'name' => 'password',
                'id'   => 'password',
                'required' => 'required',
                'label' => $this->language->general->password
            ),
            6 => Array(
                'type'  => 'submit',
                'value' => $this->language->general->submit
            ),
            7 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getAddAgentForm'
            )
        );
    }
    public function getChooseToEditForm(){
        return Array(
            0 => Array(
                'type' => 'hidden',
                'name' => 'id',
                'id'   => 'chooseIdVal'
            )
        );
    }

    public function getEditAgentForm(){
        if (isset($_REQUEST['id']))            
            $userData = $this->requestHandler->getUserInfo($_REQUEST['id']);
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => $this->requestHandler->getAgentList(),
                'name' => 'chooseId',
                'id'   => 'chooseAgentInfo',
                'value' =>isset($userData['id'])?$userData['id']:'',
                'label' => $this->language->dealerOperation->editUser
            ),
            1 => Array(
                'type'  => 'text',
                'name' => 'firstName',
                'id'   => 'firstNameE',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'value' =>isset($userData['firstName'])?$userData['firstName']:'',
                'label' => $this->language->general->firstName
            ),
            2 => Array(
                'type'  => 'text',
                'name' => 'lastName',
                'id'   => 'lastNameE',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'value' =>isset($userData['lastName'])?$userData['lastName']:'',
                'label' => $this->language->general->lastName
            ),
            3 => Array(
                'type'  => 'phone',
                'label' => $this->language->general->phone,
                'select' => Array (
                    'type'  => 'select',
                    'options' => Array(
                        '050' => '050',
                        '052' => '052',
                        '053' => '053',
                        '054' => '054',
                        '055' => '055',
                        '057' => '057',
                        '058' => '058',
                        '05522' => '05522',
                        '05544' => '05544',
                        '05566' => '05566',
                        '05588' => '05588'
                    ),
                    'name' => 'phonePre',
                    'value' =>isset($userData['phonePre'])?$userData['phonePre']:'',
                    'id'   => 'phonePreE'
                ),
                'input' => Array(
                    'name' => 'phoneNum',
                    'id'   => 'phonePostE',
                    'required' => 'required',
                    'value' =>isset($userData['phoneNum'])?$userData['phoneNum']:'',
                    'type' => 'number'
                ) 
            ),
            4 => Array(
                'type'  => 'email',
                'pattern' => '[^@]+@[^@]+\.[a-zA-Z]{2,6}',
                'name' => 'email',
                'id'   => 'emailE',
                'required' => 'required',
                'value' =>isset($userData['email'])?$userData['email']:'',
                'label' => $this->language->general->email
            ),
            5 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'usernameE',
                'required' => 'required',
                'value' =>isset($userData['userName'])?$userData['userName']:'',
                'label' => $this->language->general->username
            ),
            6 => Array(
                'type'  => 'text',
                'name' => 'password',
                'id'   => 'passwordE',
                'required' => 'required',
                'value' =>isset($userData['password'])?$userData['password']:'',
                'label' => $this->language->general->password
            ),
            7 => Array(
                'type'  => 'submit',
                'value' => $this->language->general->submit
            ),
            8 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getEditAgentForm'
            )
        );
    }
    
    public function getMainSearchForm(){
        return Array(
            0 => Array(
                'type'  => 'number',
                'name' => 'byId',
                'id'   => 'byId',
                'value' => (isset($_REQUEST['byId'])?$_REQUEST['byId']:''),
                'label' => $this->language->search->searchById
            ),
            1 => Array(
                'type'  => 'number',
                'name' => 'byLicence',
                'id'   => 'byLicence',
                'value' => (isset($_REQUEST['byLicence'])?$_REQUEST['byLicence']:''),
                'label' => $this->language->search->searchByLic
            ),
            2 => Array(
                'type'  => 'number',
                'name' => 'byInsNum',
                'id'   => 'byInsNum',
                'value' => (isset($_REQUEST['byInsNum'])?$_REQUEST['byInsNum']:''),
                'label' => $this->language->search->searchByIns
            ),
            //3 => Array(
            //    'type'  => 'month',
            //    'name' => 'bySdate',
            //    'id'   => 'bySdate',
            //    'value' => (isset($_REQUEST['bySdate'])?$_REQUEST['bySdate']:''),
            //    'placeHolder' => '2013-01',
            //    'required' => 'required',
            //    'label' => $this->language->search->searchSdate
            //),
            //4 => Array(
            //    'type'  => 'month',
            //    'name' => 'byEdate',
            //    'id'   => 'byEdate',
            //    'value' => (isset($_REQUEST['byEdate'])?$_REQUEST['byEdate']:''),
            //    'placeHolder' => '2014-01',
            //    'required' => 'required',
            //    'label' => $this->language->search->searchEdate
            //),
            5 => Array(
                'type'  => 'submit',
                'id'   => 'submitSearch',
                'value' => $this->language->search->submitSearch
            ),
            6 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getMainSearchForm'
            )
        );
    }
    
    public function getMainOpMenuForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => Array(
                    'uploadFile' => $this->language->dealerOperation->uploadFile,
                    'export'     => $this->language->dealerOperation->export,
                    'reports'    => $this->language->dealerOperation->reports,
                    'addUser'    => $this->language->dealerOperation->addUser,
                    'editUser'   => $this->language->dealerOperation->menuEdit,
                    'blockUser'  => $this->language->dealerOperation->blockUser,
                    'openUser'   => $this->language->dealerOperation->openUser
                ),
                'name' => 'dealerOperation',
                'id'   => 'dealerOperation',
                'label' => $this->language->menu->operation
            )
        );
    }
    
    public function getExportForm(){
        return Array(
            0 => Array(
                'type'  => 'datePicker',
                'month' => Array (
                    'options' => $this->month,
                    'name' => 'monthPick',
                    'value' => date('m'),
                    'id'   => 'monthPick'
                ),
                'year' => Array (
                    'options' => $this->year,
                    'name' => 'yearPick',
                    'value' => date('Y'),
                    'id'   => 'yearPick'
                ),
                'name' => 'fromMonth',
                'id'   => 'fromMonth',
                'placeHolder' => '2013-01',
                'required' => 'required',
                'label' => $this->language->general->fromDate
            ),
            1 => Array(
                'type'  => 'datePicker',
                'month' => Array (
                    'options' => $this->month,
                    'name' => 'monthPick',
                    'value' => date('m'),
                    'id'   => 'monthPick'
                ),
                'year' => Array (
                    'options' => $this->year,
                    'name' => 'yearPick',
                    'value' => date('Y'),
                    'id'   => 'yearPick'
                ),
                'name' => 'toMonth',
                'id'   => 'toMonth',
                'placeHolder' => '2013-01',
                'required' => 'required',
                'label' => $this->language->general->toDate
            ),
            2 => Array(
                'type'  => 'button',
                'id'   => 'submitExport',
                'value' => $this->language->general->submitExport
            ),
            3 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getMainSearchForm'
            )
        );
    }
    
    public function getReportsForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => Array(
                    'report1'    => $this->language->reports->renew,
                    'report2'    => $this->language->reports->unrenew
                ),
                'name' => 'reportsChoice',
                'id'   => 'reportsChoice',
                'label' => $this->language->reports->choiceOp
            ),
            1 => Array(
                'type'  => 'datePicker',
                'month' => Array (
                    'options' => $this->month,
                    'name' => 'monthPick',
                    'value' => date('m'),
                    'id'   => 'monthPick'
                ),
                'year' => Array (
                    'options' => $this->year,
                    'name' => 'yearPick',
                    'value' => date('Y'),
                    'id'   => 'yearPick'
                ),
                'name' => 'byMonth',
                'id'   => 'byMonth',
                'placeHolder' => '2013-01',
                'required' => 'required',
                'label' => $this->language->general->fromDate
            ),
            2 => Array(
                'type'  => 'button',
                'id'   => 'submitReports',
                'value' => $this->language->reports->submitReport
            ),
            3 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getMainSearchForm'
            )
        );
    }
    
    public function getMainAdminMenuForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => Array(
                    'menuAdd' => $this->language->dealerOperation->menuAdd,
                    'menuBlock' => $this->language->dealerOperation->menuBlock
                ),
                'name' => 'adminOperation',
                'id'   => 'adminOperation',
                'label' => $this->language->menu->operation
            ),
            1 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getMainOpMenuForm'
            )
        );
    }
    
    public function getblockMenuForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => $this->requestHandler->getNonBlockAgentList(),
                'name' => 'chooseAgentBlock',
                'id'   => 'chooseAgentBlock',
                'label' => $this->language->dealerOperation->chooseAgent
            ),
            1 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getblockMenuForm'
            ),
            2 => Array(
                'type' => 'submit',
                'value' => $this->language->dealerOperation->menuBlock
            )
        );
    }
    
    public function getopenMenuForm(){
        return Array(
            0 => Array(
                'type'  => 'select',
                'options' => $this->requestHandler->getBlockAgentList(),
                'name' => 'chooseAgentOpen',
                'id'   => 'chooseAgentOpen',
                'label' => $this->language->dealerOperation->openAgent
            ),
            1 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getopenMenuForm'
            ),
            2 => Array(
                'type' => 'submit',
                'value' => $this->language->dealerOperation->menuOpen
            )
        );
    }
    
    public function getLanguageForm(){
        return Array(
            0 => Array(
                'type'  => 'button',
                'name'  => 'engChange',
                'id'    => 'engChange',
                'value' => 'eng'
            ),
            1 => Array(
                'type'  => 'button',
                'name'  => 'hebChange',
                'id'    => 'hebChange',
                'value' => '���'
            ),
            2 => Array(
                'type' => 'hidden',
                'name' => 'formAction',
                'value' => 'getLanguageForm'
            )
        );
    }
    
        public function getInstallForm(){
        return Array(
            0 => Array(
                'type'  => 'text',
                'name' => 'dataBaseHost',
                'id'   => 'dataBaseHost',
                'required' => 'required',
                'label' => 'Set DataBase Host',
                'placeHolder'=>'localhost'
            ),
            1 => Array(
                'type'  => 'text',
                'name' => 'dataBaseName',
                'id'   => 'dataBaseName',
                'required' => 'required',
                'label' => 'Set DataBase Name',
                'placeHolder'=>'insurance'
            ),
            2 => Array(
                'type'  => 'text',
                'name' => 'dataBaseUser',
                'id'   => 'dataBaseUser',
                'required' => 'required',
                'label' => 'Set DataBase UserName',
                'placeHolder'=>'root'
            ),
            3 => Array(
                'type'  => 'text',
                'name' => 'dataBasePass',
                'id'   => 'dataBasePass',
                'label' => 'Set DataBase Password',
                'placeHolder'=>'123456'
            ),
            4 => Array(
                'type'  => 'text',
                'name' => 'firstName',
                'id'   => 'firstName',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'placeHolder'=>'Dor',
                'label' => 'Admin '.$this->language->general->firstName
            ),
            5 => Array(
                'type'  => 'text',
                'name' => 'lastName',
                'id'   => 'lastName',
                'placeHolder'=>'Cohen',
                'pattern' => '^[\w\u0590-\u05FF][^0-9 *]+$',
                'required' => 'required',
                'label' => 'Admin '.$this->language->general->lastName
            ),
            6 => Array(
                'type'  => 'phone',
                'label' => 'Admin '.$this->language->general->phone,
                'value' => '052',
                'select' => Array (
                    'type'  => 'select',
                    'options' => Array(
                        '050' => '050',
                        '052' => '052',
                        '053' => '053',
                        '054' => '054',
                        '055' => '055',
                        '057' => '057',
                        '058' => '058',
                        '05522' => '05522',
                        '05544' => '05544',
                        '05566' => '05566',
                        '05588' => '05588'
                    ),
                    'name' => 'phonePre',
                    'id'   => 'phonePre'
                ),
                'input' => Array(
                    'name' => 'phoneNum',
                    'id'   => 'phonePost',
                    'placeHolder'=>'6022061',
                    'required' => 'required',
                    'type' => 'number'
                ) 
            ),
            7 => Array(
                'type'  => 'email',
                'pattern' => '[^@]+@[^@]+\.[a-zA-Z]{2,6}',
                'name' => 'email',
                'placeHolder'=>'dorc.tech@gmail.com',
                'id'   => 'email',
                'required' => 'required',
                'label' => 'Admin '.$this->language->general->email
            ),
            8 => Array(
                'type'  => 'text',
                'name' => 'userName',
                'id'   => 'username',
                'placeHolder'=>'Admin',
                'required' => 'required',
                'label' => 'Admin '.$this->language->general->username
            ),
            9 => Array(
                'type'  => 'password',
                'name' => 'password',
                'id'   => 'password',
                'placeHolder'=>'123456',
                'required' => 'required',
                'label' => 'Admin '.$this->language->general->password
            ),
            10 => Array(
                'type'  => 'submit',
                'name' => 'submit',
                'value'   => 'submit'
            ),
        );
    }
   
}
?>