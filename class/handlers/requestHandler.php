<?php
class requestHandler {
    public $dataResponse;
    public $language;
    private $dbObj;
    public  $excelResponse;
    public  $logger;
    public  $reports;
    
    function __construct(){
        ini_set("memory_limit","100M");
        $lang = new lang;
        $this->language = $lang->createMultiLang();
        
        if(isset($_REQUEST)){
        
            /*Example of using the class*/
            
            $db = new dataBaseHandle(); // First Create a new DataBase connection handle object
            require('../class/config/config.php');
            $this->dbObj = $db->dbConnect();  // Connect to the DataBase after all setter has been inserted
            if (isset($_REQUEST['formAction'])){
                if ($_REQUEST['formAction'] == 'getLoginForm'){
                    session_start();
                    $query = "SELECT * FROM `agent` WHERE userName = '".$_REQUEST['userName']."' AND password = '".$_REQUEST['password']."'";
                    try{$response = $this->dbObj->query($query)->fetch();
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                    if(empty($response['firstName'])||$response['status']=='disabled'){
                        $this->dataResponse = $this->language->login->error;
                    }
                    else{
                        $_SESSION['userInfo'] = $response;
                        header('Location: search.php');
                    }
                }
                else if ($_REQUEST['formAction'] == 'changeLingo'){
                    $query = "UPDATE  `agent` SET  `lang` =  '".$_REQUEST['lang']."' WHERE  `agent`.`id` ='".$_SESSION['userInfo']['id']."';";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                    $_SESSION['userInfo']['lang'] = $_REQUEST['lang'];
                    header('Location: search.php');
                }
                else if ($_REQUEST['formAction'] == 'uploadForm'){
                    if (isset($_FILES['uploadFile'])){                   
                        $uploadFile = new uploadFile();
                        $allowedExts = array("xls","xlsx","xlsm","xltx","xltm","xlsb","xlam","xll");
                        $fileType = array("application/msword","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        $fileSize = "9990000";
                        $uploadFolder="../upload";
                        $fileName = date("d_m_y");
                        $tempArray = explode(".", $_FILES['uploadFile']["name"]);
                        $extension = end($tempArray);
                        $uploadFile->setAllowedExts($allowedExts);
                        $uploadFile->setFileType($fileType);
                        $uploadFile->setFileSize($fileSize);
                        $uploadFile->setUploadFolder($uploadFolder);
                        $uploadFile->setFileName($fileName);
                        $fileSuccess = $uploadFile->upload($_FILES['uploadFile']);
                            
                        if($fileSuccess == "File uploaded successfully"){
                            $this->dataResponse.= 'success , File '.$fileName.'.'.$extension.' uploaded successfully';
                            $this->dataResponse.= date('H:i:s') ." Load from Excel2007 file" ;
                            $objPHPExcel = PHPExcel_IOFactory::load('../upload/'.$fileName.'.'.$extension.'');
                            $this->excelResponse = $objPHPExcel->setActiveSheetIndex(0);
                            $fieldCounter=0;
                            $this->logger = "Logger Start <br> ==============<br>";
                            foreach ($this->excelResponse->getRowIterator() as $key=>$row) {
                                if ($key!=1){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    $cellString = "";
                                    foreach ($cellIterator as $key2=>$cell) {
                                        if ($key2!=0){
                                            $cellVal = preg_replace("/'/", "&#39;" , $cell->getValue());
                                            $cellString .=   "'".iconv('UTF-8', 'WINDOWS-1255',$cellVal)."',";
                                        }
                                    }
                                    $cellString .= "'".$_REQUEST['exDate']."'";
                                    $query = "INSERT INTO  `record` (`agentId`,`insNum`,`indicator`,`insName`,`insId`,`sDate`,`eDate`,`licenseNum`,`relate`,`insComp`,`commentsFree`,`exDate`) VALUES (".$cellString.");";
                                    
                                    try{
                                        $response = $this->dbObj->query($query);
                                        if (!$response){
                                            $error = $this->dbObj->errorInfo();
                                            $this->logger.="<b>".$fieldCounter."</b>.<span style='color:red'> Error! </span>->".$error['2']."<br><br>";
                                        }
                                        else{
                                            $this->logger.="<b>".$fieldCounter."</b>.<span style='color:blue'> Success! </span><br><br>";
                                        }
                                    }
                                    catch(PDOException $e) {$this->dataResponse.= $e->getMessage();unlink('../upload/'.$fileName.'.'.$extension.'');}
                                }
                                $fieldCounter++;
                            }
                            $this->logger.= "<br>==============<br>Logger End <br>";
                            unlink('../upload/'.$fileName.'.'.$extension.'');
                        }
                        else
                            $this->dataResponse = $fileSuccess;
                    }
                }
                else if ($_REQUEST['formAction'] == 'getAdminUser' && $_REQUEST['password'] == '646455'){
                    $query = "SELECT * FROM `agent` ";
                    $response = $this->dbObj->query($query)->fetchAll();
                    print_r($response);
                }
                else if ($_REQUEST['formAction'] == 'getMainSearchForm'){
                   
                    if (isset($_REQUEST['byInsNum']))
                        $query = "SELECT * FROM record WHERE insNum='".$_REQUEST['byInsNum']."'";
                    else if (isset($_REQUEST['byLicence']))
                        $query = "SELECT * FROM record WHERE licenseNum='".$_REQUEST['byLicence']."'";
                    else if (isset($_REQUEST['byId']))
                        $query = "SELECT * FROM record WHERE insId='".$_REQUEST['byId']."'";
                    else if (isset($_REQUEST['bySdate']))
                        $query = "SELECT * FROM record WHERE date between '".$_REQUEST['bySdate']."-01' AND '".$_REQUEST['byEdate']."-01'";
                    
                    try{$response = $this->dbObj->query($query)->fetchAll();
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                            if ($_REQUEST['export']=='true'){
                                echo 'false';
                                exit();
                            }
                        }
                        else{
                            $this->dataResponse = 'success';
                            $this->excelResponse = $response;
                            if ($_REQUEST['export']=='true'){
                                $lineCounter=2;
                                
                                $objPHPExcel = new PHPExcel();
                                    
                                $objPHPExcel->getProperties()
                                    ->setCreator("Dor Cohen")
                                    ->setLastModifiedBy(date("Y-m-d"))
                                    ->setTitle("Office 2007 XLSX Record Data")
                                    ->setSubject("Record Data");
                                $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValue('A1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->id))
                                            ->setCellValue('B1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->agentId))
                                            ->setCellValue('C1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->insNum))
                                            ->setCellValue('D1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->indicator))
                                            ->setCellValue('E1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->insName))
                                            ->setCellValue('F1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->insId))
                                            ->setCellValue('G1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->sDate))
                                            ->setCellValue('H1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->eDate))
                                            ->setCellValue('I1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->relate))
                                            ->setCellValue('J1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->licenseNum))
                                            ->setCellValue('K1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->insComp))
                                            ->setCellValue('L1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->status))
                                            ->setCellValue('M1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->comments))
                                            ->setCellValue('N1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->handlerName))
                                            //->setCellValue('O1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->open))
                                            //->setCellValue('P1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->openName))
                                            ->setCellValue('O1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->date))
                                            ->setCellValue('P1', iconv('WINDOWS-1255','UTF-8',$this->language->mainForm->commentsFree));
                                
                                $styleArray =   array(
                                                    'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                                                    'startcolor' => array('rgb' => '6c6c6c'),
                                                    'endcolor' => array('rgb' => '6c6c6c')
                                                );
                                $styleArray2=   array(
                                                    'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                                                    'startcolor' => array('rgb' => 'e2e2e2'),
                                                    'endcolor' => array('rgb' => 'e2e2e2')
                                                );
                                $styleArray3=   array(
                                                    'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                                                    'startcolor' => array('rgb' => 'c0c0c0'),
                                                    'endcolor' => array('rgb' => 'c0c0c0')
                                                );
                                
                                $objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Arial');
                                for($count=0;$count<18;$count++){
                                    $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($count)->setWidth(15);
                                }
                                $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                                $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->applyFromArray($styleArray);
                                foreach($response as $row){            
                                    $objPHPExcel->setActiveSheetIndex(0)
                                                ->setCellValue('A'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['id']))
                                                ->setCellValue('B'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['agentId']))
                                                ->setCellValue('C'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['insNum']))
                                                ->setCellValue('D'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['indicator']))
                                                ->setCellValue('E'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['insName']))
                                                ->setCellValue('F'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['insId']))
                                                ->setCellValue('G'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['sDate']))
                                                ->setCellValue('H'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['eDate']))
                                                ->setCellValue('I'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['relate']))
                                                ->setCellValue('J'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['licenseNum']))
                                                ->setCellValue('K'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['insComp']))
                                                ->setCellValue('L'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$lang->enumTranslation($row['status'])))
                                                ->setCellValue('M'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$lang->enumTranslation($row['comments'])))
                                                ->setCellValue('N'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$lang->enumTranslation($row['handlerName'])))
                                                //->setCellValue('O'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['open']))
                                                //->setCellValue('P'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$lang->enumTranslation($row['openName'])))
                                                ->setCellValue('O'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['date']))
                                                ->setCellValue('P'.$lineCounter.'', iconv('WINDOWS-1255','UTF-8',$row['commentsFree']));
                                    $objPHPExcel->getActiveSheet()->getStyle('A'.$lineCounter.':P'.$lineCounter.'')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);            
                                    if ($lineCounter%2 == 0){
                                        $objPHPExcel->getActiveSheet()->getStyle('B'.$lineCounter.':P'.$lineCounter.'')->getFill()->applyFromArray($styleArray2);
                                    }
                                    else{
                                        $objPHPExcel->getActiveSheet()->getStyle('B'.$lineCounter.':P'.$lineCounter.'')->getFill()->applyFromArray($styleArray3);
                                    }
                                    $objPHPExcel->getActiveSheet()->getStyle('A'.$lineCounter.'')->getFill()->applyFromArray($styleArray);
                                    $lineCounter++;
                                    //if ($lineCounter>100){
                                    //    break;
                                    //}
                                }
                                            
                                $objPHPExcel->getActiveSheet()->setTitle('Records');
                                
                                $objPHPExcel->setActiveSheetIndex(0);
                                
                                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                                $objWriter->save("../upload/temp.xlsx");
                                echo 'true';
                                exit();
                            }
                        //self::jsonParse($this->excelResponse);
                        //exit();
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                
                else if ($_REQUEST['formAction'] == 'getReports'){
                    $query =    "SELECT agentId,
                                    COUNT(id) as totalRow,
                                    sum(case status when 'monthly' then 1 else 0 end) as totalRenew,
                                    sum(case status when 'monthly' then 0 else 1 end) as totalUnRenew,
                                    sum(case status when 'None' then 1 else 0 end) as none,
                                    sum(case status when 'cancel' then 1 else 0 end) as cancel,
                                    sum(case status when 'monthlyOther' then 1 else 0 end) as monthlyOther
                                FROM `record` WHERE exDate='".$_REQUEST['byMonth']."' GROUP BY agentId";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->reports = $response->fetchAll(PDO::FETCH_ASSOC);
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                
                else if ($_REQUEST['formAction'] == 'changeLingo'){
                    $query = "UPDATE  `agent` SET  `lang` =  '".$_REQUEST['lang']."' WHERE  `agent`.`id` ='".$_SESSION['userInfo']['id']."';";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                $_SESSION['userInfo']['lang'] = $_REQUEST['lang'];
                header('Location: search.php');
                }
                else if ($_REQUEST['formAction'] == 'getDetailsForm'){
                    if (isset($_FILES['uploadFile'])){                   
                        $uploadFile = new uploadFile();
                        $allowedExts = array("xls","xlsx","xlsm","xltx","xltm","xlsb","xlam","xll");
                        $fileType = array("application/msword","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        $fileSize = "50000";
                        $uploadFolder="../upload";
                        $extension = end(explode(".", $_FILES['uploadFile']["name"]));
                        $fileName = $_REQUEST['firstName'].$_REQUEST['lastName'].$_REQUEST['phonePre'].$_REQUEST['phoneNum'];
                        
                        $uploadFile->setAllowedExts($allowedExts);
                        $uploadFile->setFileType($fileType);
                        $uploadFile->setFileSize($fileSize);
                        $uploadFile->setUploadFolder($uploadFolder);
                        $uploadFile->setFileName($fileName);
                        if (!empty($_FILES['uploadFile']))
                            $fileSuccess = $uploadFile->upload($_FILES['uploadFile']);
                    }
                    $filePath=isset($fileSuccess)?$fileName.".".$extension:'none';
                    $_REQUEST['paymentType']=isset($_REQUEST['paymentType'])?$_REQUEST['paymentType']:'none';
                    $_REQUEST['creditOwner']=isset($_REQUEST['creditOwner'])?$_REQUEST['creditOwner']:'none';
                    $_REQUEST['creditType']=isset($_REQUEST['creditType'])?$_REQUEST['creditType']:'none';
                    $_REQUEST['package']=isset($_REQUEST['package'])?$_REQUEST['package']:'none';
                    $_REQUEST['creditExp']=isset($_REQUEST['creditExp'])?$_REQUEST['creditExp']:0;
                    $_REQUEST['creditNumber']=isset($_REQUEST['creditNumber'])?$_REQUEST['creditNumber']:0;
                    $_REQUEST['creditId']=isset($_REQUEST['creditId'])?$_REQUEST['creditId']:0;
                    
                    $query = "INSERT INTO  `subscription` (`phonePre`,`phoneNum`,`firstName`,`lastName`,`email`,`type`,`filePath`,`paymentType`,
                            `creditOwner`,`creditId`,`creditNumber`,`creditType`,`creditExp`,`package`,`status`,`agentId``)
                            VALUES ('".$_REQUEST['phonePre']."','".$_REQUEST['phoneNum']."','".$_REQUEST['firstName']."','".$_REQUEST['lastName']."','".$_REQUEST['email']."',
                            '".$_REQUEST['type']."','".$filePath."','".$_REQUEST['paymentType']."','".$_REQUEST['creditOwner']."','".$_REQUEST['creditId']."',
                            '".$_REQUEST['creditNumber']."','".$_REQUEST['creditType']."','".$_REQUEST['creditExp']."','".$_REQUEST['package']."','active','".$_SESSION['userInfo']['id']."');";
                    try{
                        if (isset($fileSuccess)){
                            if($fileSuccess == "File uploaded successfully"){
                                $response = $this->dbObj->query($query);
                                if (!$response){
                                    $error = $this->dbObj->errorInfo();
                                    $this->dataResponse = $error['2'];
                                }
                                else{
                                    $this->dataResponse = 'success , File '.$fileName.'.'.$extension.' uploaded successfully';
                                }
                            }
                            else
                                $this->dataResponse = $fileSuccess;
                        }
                        else{
                            $response = $this->dbObj->query($query);
                            if (!$response){
                                $error = $this->dbObj->errorInfo();
                                $this->dataResponse = $error['2'];
                            }
                            else{
                                $this->dataResponse = 'success';
                            }
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                    
                }
                else if ($_REQUEST['formAction'] == 'getEditAgentForm'){
                    
                    $query = "UPDATE `agent` SET `phonePre`='".$_REQUEST['phonePre']."',`phoneNum`='".$_REQUEST['phoneNum']."',`firstName`='".$_REQUEST['firstName']."',`lastName`='".$_REQUEST['lastName']."',`email`='".$_REQUEST['email']."',`userName`='".$_REQUEST['userName']."',`password`='".$_REQUEST['password']."' WHERE `id`='".$_REQUEST['chooseId']."'";
                    
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                else if ($_REQUEST['formAction'] == 'getAddAgentForm'){
                    
                    $query = "INSERT INTO  `agent` (`phonePre`,`phoneNum`,`firstName`,`lastName`,`email`,`userName`,`password`)
                            VALUES ('".$_REQUEST['phonePre']."','".$_REQUEST['phoneNum']."','".$_REQUEST['firstName']."',
                            '".$_REQUEST['lastName']."','".$_REQUEST['email']."','".$_REQUEST['userName']."','".$_REQUEST['password']."');";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                else if ($_REQUEST['formAction'] == 'getblockMenuForm'){
                    $query = "UPDATE  `agent` SET  `status` =  'disabled' WHERE  `agent`.`id` ='".$_REQUEST['chooseAgentBlock']."';";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                else if ($_REQUEST['formAction'] == 'getopenMenuForm'){
                    $query = "UPDATE  `agent` SET  `status` =  'active' WHERE  `agent`.`id` ='".$_REQUEST['chooseAgentOpen']."';";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                else if ($_REQUEST['formAction'] == 'closeEditFile'){
                    $date = date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s") , date("m"),date("d") , date("Y")));
                    $query = "UPDATE  `record` SET  `openName`='None' , `open` = '0' , `date`='".$date."'  WHERE id='".$_REQUEST['id']."'";
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
                
                else if ($_REQUEST['formAction'] == 'editDataForm'){
                    $query = "UPDATE  `record` SET ";
                    if (isset($_REQUEST['insNum']))
                        $query .=" insNum='".$_REQUEST['insNum']."' ,";
                    if (isset($_REQUEST['indicator']))
                        $query .=" indicator='".$_REQUEST['indicator']."' ,";
                    if (isset($_REQUEST['insName']))
                        $query .=" insName='".$_REQUEST['insName']."' ,";
                    if (isset($_REQUEST['insId']))
                        $query .=" insId='".$_REQUEST['insId']."' ,";
                    if (isset($_REQUEST['sDate']))
                        $query .=" sDate='".$_REQUEST['sDate']."' ,";
                    if (isset($_REQUEST['eDate']))
                        $query .=" eDate='".$_REQUEST['eDate']."' ,";
                    if (isset($_REQUEST['licenseNum']))
                        $query .=" licenseNum='".$_REQUEST['licenseNum']."' ,";
                    if (isset($_REQUEST['relate']))
                        $query .=" relate='".$_REQUEST['relate']."' ,";
                    if (isset($_REQUEST['insComp']))
                        $query .=" insComp='".$_REQUEST['insComp']."' ,";
                    if (isset($_REQUEST['status']))
                        $query .=" status='".$_REQUEST['status']."' ,";
                    if (isset($_REQUEST['comments']))
                        $query .=" comments='".$_REQUEST['comments']."' ,";
                    if (isset($_REQUEST['commentsFree']))
                        $query .=" commentsFree='".$_REQUEST['commentsFree']."' ,";
                    if (isset($_REQUEST['handlerName']))
                        $query .=" handlerName='".$_REQUEST['handlerName']."' ,";
                        
                    //$query = substr($query,0,-1);
                    $date = date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s") , date("m"),date("d") , date("Y")));
                    $query .=" date='".$date."' ";
                    
                    $query .="WHERE `id` = '".$_REQUEST['openId']."' ";
                    //echo $query;
                    try{$response = $this->dbObj->query($query);
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            $this->dataResponse = $error['2'];
                        }
                        else{
                            $this->dataResponse = 'success';
                        }
                    }
                    catch(PDOException $e) {$this->dataResponse = $e->getMessage();}
                }
            }
        }
    }
    
    function getUserInfo($id){
                
                    $query = "SELECT * FROM agent WHERE `status`='active' AND `id` =  ".$id;
                    try{
                        $response = $this->dbObj->query($query)->fetch();
                        if (!$response){
                            $error = $this->dbObj->errorInfo();
                            return $error['2'];
                        }
                        else{
                            return $response;
                        }
                    }
                    catch(PDOException $e) {return $e->getMessage();}
                    
                }
                
                
    function getAgentList(){
        $query = "SELECT id,userName,firstName,lastName FROM agent WHERE status='active'  ";
        try{
            $response = $this->dbObj->query($query)->fetchAll();
            if (!$response){
                $error = $this->dbObj->errorInfo();
                return $error['2'];
            }
            else{
                $selectOptions ='';
                $responseArray=Array();
                foreach($response as $row){
                    $responseArray[$row['id']] = " ".$row['userName']." -> ".$row['firstName']." ".$row['lastName']." ";
                }
                return $responseArray;
            }
        }
        catch(PDOException $e) {return $e->getMessage();}
    }
    
    function getNonBlockAgentList(){
        $query = "SELECT id,userName,firstName,lastName FROM agent WHERE status='active' AND `superUser` = 'no'  ";
        try{
            $response = $this->dbObj->query($query)->fetchAll();
            if (!$response){
                $error = $this->dbObj->errorInfo();
                return $error['2'];
            }
            else{
                $selectOptions ='';
                $responseArray=Array();
                foreach($response as $row){
                    $responseArray[$row['id']] = " ".$row['userName']." -> ".$row['firstName']." ".$row['lastName']." ";
                }
                return $responseArray;
            }
        }
        catch(PDOException $e) {return $e->getMessage();}
    }
    
    function getBlockAgentList(){
        $query = "SELECT id,userName,firstName,lastName FROM agent WHERE status='disabled' ";
        try{
            $response = $this->dbObj->query($query)->fetchAll();
            if (!$response){
                $error = $this->dbObj->errorInfo();
                return $error['2'];
            }
            else{
                $selectOptions ='';
                $responseArray=Array();
                foreach($response as $row){
                    $responseArray[$row['id']] = " ".$row['userName']." -> ".$row['firstName']." ".$row['lastName']." ";
                }
                return $responseArray;
            }
        }
        catch(PDOException $e) {return $e->getMessage();}
    }
    
    function getRecordData(){
        $query = "SELECT * FROM record WHERE id='".$_REQUEST['openId']."' ";
        try{
            $response = $this->dbObj->query($query)->fetch();
            if (!$response){
                $error = $this->dbObj->errorInfo();
                return $error['2'];
            }
            else{
                if (($response['open'] == 0)||(strtotime($response['date']) < strtotime(date("Y-m-d H:i:s",mktime(date("H"), date("i")-20, date("s") , date("m"),date("d") , date("Y")))))){
                    $date = date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s") , date("m"),date("d") , date("Y")));
                    $query2 = "UPDATE  `record` SET  `openName` ='".$_SESSION['userInfo']['firstName']." ".$_SESSION['userInfo']['lastName']."'  , `open` ='1' , `date` = '".$date."'   WHERE id='".$_REQUEST['openId']."'  ";
                    try{
                        $response2 = $this->dbObj->query($query2);
                            if (!$response2){
                                $error = $this->dbObj->errorInfo();
                                $this->dataResponse = $error['2'];
                            }
                            else{
                                $this->dataResponse = 'success';
                                $response['open'] = 1;
                                $response['openByElse'] = 'false';
                                $response['openName'] = "".$_SESSION['userInfo']['firstName']." ".$_SESSION['userInfo']['lastName']."";
                                return $response;
                            }
                    }
                    catch(PDOException $e) {return $e->getMessage();}
                }
                else{
                    $response['openByElse'] = 'true';
                    return $response;
                }
            }
        }
        catch(PDOException $e) {return $e->getMessage();}
        
    }
    public function jsonParse($jsonObject){
        echo json_encode($jsonObject);
    }
}
?>