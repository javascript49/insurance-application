var timer = 1200;
var setTime;
var javascriptPages = {
    admin : function(){
        mainAct();
        
        var monthPick= $("select[name='monthPick']").attr('value');
        var yearPick = $("select[name='yearPick']").attr('value');
        
        $("select[name='yearPick']").val(yearPick);
        $("select[name='monthPick']").val(monthPick);
        $('#exDate,#byMonth,#fromMonth,#toMonth').val(yearPick+'-'+monthPick);
        
        $("select[name='yearPick'] , select[name='monthPick']").change(function () {
            var monthPick = $(this).parent().find('select[name="monthPick"]').val();
            var yearPick  = $(this).parent().find('select[name="yearPick"]').val();
            $(this).parent().find('input[type="hidden"]').val(yearPick+'-'+monthPick);
        });
        
        $('#loggerOpen').click(function () {
            var content = $(this).attr('loggerRes');
            createPopup("<html><body>"+content+"</body></html>",'400px','600px','logger');
        });
            
        $('#submitExport').click(function () {
            var fromMonth   = new Date($("#fromMonth").val());
            var toMonth     = new Date($("#toMonth").val());
            if ($(this).val() == message.buttonSec){
                window.location.href='../upload/temp.xlsx';
                $(this).val(message.submitExport);
            }
            else{
                if (parseInt(fromMonth.getTime()) > parseInt(toMonth.getTime())){
                    alert(message.dateAlert);
                }
                else{
                    $(this).val(message.buttonOne);
                    //window.open('search.php?formAction=getMainSearchForm&bySdate='+$("#fromMonth").val()+'&byEdate='+$("#toMonth").val()+'&export=true','','width=50,height=50,scrollbars=1');
                    var AJAX=new XMLHttpRequest();
                    AJAX.onreadystatechange= function() {
                        if (AJAX.readyState!=4){
                            return false;
                        }
                        else {
                            if (AJAX.responseText.indexOf("true")>0) {
                                $('#submitExport').val(message.buttonSec);
                            }
                            else{
                                alert(message.noData);
                                $('#submitExport').val(message.submitExport);
                            }
                        }
                    }
                    AJAX.open("GET", 'search.php?formAction=getMainSearchForm&bySdate='+$("#fromMonth").val()+'&byEdate='+$("#toMonth").val()+'&export=true', false);
                    AJAX.send(null);
                }
            }
        });
        
        $('#submitReports').click(function () {
            var reportType = $("#reportsChoice").val();
            var byMonth = $("#byMonth").val();
            window.open('reports.php?reportType='+reportType+'&byMonth='+byMonth+'&formAction=getReports','','width=800,height=800,scrollbars=0');
        });
        
        $('.mainMenu .menuItem:eq(1)').addClass('activeMenu');
        disableForm($(".adminForms"));
        $(".adminForms ").hide();
        
        if ($('#chooseAgentInfo').attr('value').length > 0) {
            $('#dealerOperation').val('editUser');
            enableForm($("form[name='editUser']"));
            $("form[name='editUser']").show();
            $('#chooseAgentInfo').val($('#chooseAgentInfo').attr('value'));
        }
        else{
            enableForm($("form[name='uploadFile']"));
            $("#chooseAgentInfo").prop("selectedIndex", -1);
            $("form[name='uploadFile']").show();
        }
       
        $("#chooseAgentOpen,#chooseAgentBlock").prop("selectedIndex", -1);
        
        $("#dealerOperation").change(function () {
            disableForm($(".adminForms"));
            $(".adminForms ").hide();
            enableForm($("form[name='"+$(this).val()+"']"));
            $("form[name='"+$(this).val()+"']").show();

        });
        
        $('#chooseAgentInfo').change(function () {
            $('#chooseIdVal').val($(this).val());
            $('form[name="ChooseToEdit"]').submit(); 
        });
        
        
    },
    reports    : function(){
        $('#printReport').click(function () {
            window.print();
        });
    },
    mainSearch : function(){
        $('.mainMenu .menuItem:eq(0)').addClass('activeMenu');
        mainAct();
        
        $('.openForEdit').click(function () {
            var conf=confirm(message.areYouSure);
            if (conf==true){
                $('#loadOverLay').show();
                $.ajax({
                    url:'editPopUp.php',
                    type: 'POST',
                    data : 'openId='+$(this).attr('num')+''
                })
                .done(function (data) {
                    $('.ajaxCont').html(data);
                    $('#loadOverLay').hide();
                    $('.ajaxCont').show();
                    javascriptPages.popUp(); 
                });
            }
        });
        
        $.each ($('.mainSearchForm input[type="number"],.mainSearchForm input[type="month"]'),function (){
            if($(this).val().length > 0){
                $('.mainSearchForm input[type="number"],.mainSearchForm input[type="month"]').attr('disabled','disabled');
                if ($(this).attr('type') == 'month') {
                    $('.mainSearchForm input[type="month"]').removeAttr('disabled');
                }
                else
                    $(this).removeAttr('disabled');
                
            }
        });
        
        $('.mainSearchForm input[type="number"]').on("change keyup", function (e) {
            $(this).val($(this).val().replace(/[^0-9]/g,''));
            if ($(this).val().length == '0') {
                $('.mainSearchForm input[type="number"],.mainSearchForm input[type="month"]').removeAttr('disabled');
            }
            else{
                var selectedId =  $(this).attr('id');
                $('.mainSearchForm input[type="number"][id!="'+selectedId+'"],.mainSearchForm input[type="month"]').attr('disabled','disabled');
                //$(this).removeAttr('disabled');
            }
        });
        
        $('.mainSearchForm input[type="month"]').change (function (e) {
            if (($('.mainSearchForm #bySdate').val().length == '0')&&($('.mainSearchForm #byEdate').val().length == '0')) {
                $('.mainSearchForm input[type="number"]').removeAttr('disabled');
            }
            else{
                $('.mainSearchForm input[type="number"]').attr('disabled','disabled');
                $(this).removeAttr('disabled');
            }
        });
        
    },
    popUp : function () {
        mainAct();
        window.onbeforeunload=goodbye;
        $('#openAllFields').on('click',function(){
            $('.editDataForm').find('input[type="number"],input[type="text"]').removeAttr('disabled');
            $('.editDataForm').find('#openName,#openField,#exDate').attr('disabled','disabled');
        });
        
        $('.closeMark').on('click',function(){
            goodbye();
            $('.ajaxCont').hide();
        });
        
        $('#forceFileOpen').on('click',function(){
            var conf=confirm(message.areYouSureOpen);
            if (conf==true){
                $('.editDataForm').find("#openByElse").val('false');
                goodbye();
                $('#loadOverLay').show();
                $.ajax({
                    url:'editPopUp.php',
                    type: 'POST',
                    data : 'openId='+$('.editDataForm').find("#openId").val()+''
                })
                .done(function (data) {
                    $('.ajaxCont').html(data);
                    $('#loadOverLay').hide();
                    javascriptPages.popUp();
                });
            }
        });
        
        $('#subData').on('click',function(){
            $('#loadOverLay').show();
            var editData = decodeURIComponent($('.editDataForm').serialize());
            $.ajax({
                    url:'editPopUp.php?'+editData+'',
                    type: 'POST'
            })
            .done(function (data) {
                    goodbye();
                    $('.ajaxCont').hide();
                    $('#loadOverLay').hide();
            });
        });
        
        $('.editDataForm').find("#comments").val($("#comments").attr('value'));
        $('.editDataForm').find("#indicator").val($("#indicator").attr('value'));
        $('.editDataForm').find("#status").val($("#status").attr('value'));
        $('.editDataForm').find("#relate").val($("#relate").attr('value'));
        $('.editDataForm').find("#insComp").val($("#insComp").attr('value'));
        if ($('.editDataForm').find('#openByElse').val() == 'false'){
            $('.editDataForm').find('#subData').removeAttr('disabled');
            var originalValue = $('#subData').val();
            setTime = setInterval(function () {
                timer--;
                if (timer<0) {
                    goodbye();
                    $('.ajaxCont').hide();
                    //window.close();
                }
                var seconds = Math.floor(timer%60);
                if (seconds < 10) {
                    seconds = "0"+Math.floor(timer%60);
                }
                $('.editDataForm').find('#subData').val(originalValue+" "+Math.floor(timer/60)+":"+seconds);
            },1000);    
        }
    }
}

function mainAct() {
    if($('body').css('float') == 'right')
        $('.langSelectBox,.closeMark').css('float','left');
    else if ($('body').css('float') == 'left')
        $('.langSelectBox,.closeMark').css('float','right');
    $('#langFormEng').on('click',function () {
        $('#lingoVal').val('eng');
        $('#updateLingo').submit();
    });
    $('#langFormHeb').on('click',function () {
        $('#lingoVal').val('heb');
        $('#updateLingo').submit();
    });
}
function goodbye() {
    clearInterval(setTime);
    var openId=$('.editDataForm').find('#openId').val();
    var openByElse=$('.editDataForm').find('#openByElse').val();
    if (openByElse == "false") {
        
        var AJAX=new XMLHttpRequest();  
        AJAX.onreadystatechange= function() { if (AJAX.readyState!=4) return false; }
        AJAX.open("GET", '../content/search.php?formAction=closeEditFile&id='+openId, false);
        AJAX.send(null);
        
    }
}
function createPopup(content,width,height,name){
    var popup = open("", name , "width="+width+",height="+height+"");
    popup.document.write(content);
    
}
function disableForm(formObj) {
    formObj.find('input, textarea, select').attr('disabled','disabled');
}
function enableForm(formObj) {
    formObj.find('input, textarea, select').removeAttr('disabled');
}

